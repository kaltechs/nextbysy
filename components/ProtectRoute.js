import { useUser } from "../hooks/useUser";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";

export const ProtectRoute = ({ children }) => {
  const { user, isLoading, isError } = useUser();
  const router = useRouter();
  
  if (
    isError &&
    isError.response.data.message === "User not found." &&
    window.location.pathname.startsWith("/admin/")
  ) {
    console.log('You are signed out...sorry');
    router.replace("/auth/login");
  }

  if (isLoading || (!user && window.location.pathname !== "/auth/login")) {
    const SplashScreen = dynamic(() => import("./SplashScreen"));
    return <SplashScreen />;
  }

  return children;
};
