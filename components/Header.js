import Link from "next/link";
import Router from "next/router";
import { useState } from "react";
import { PUBLIC_MENUS } from "../util/enums";
import ActiveLink from "./ActiveLink";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container,
  Row,
  Col,
  Form,
  Label,
  Input,
  Button,
  Badge,
} from "reactstrap";

const Header = () => {
  const [collapsed, setCollapsed] = useState(false);
  const [parentName, setParentName] = useState(false);

  return (
    <header>
      <Navbar expand='lg'>
        <Container fluid={true}>
          <div className='d-flex align-items-center'>
            <Link href='/' passHref>
              <a className='py-1 navbar-brand'>
                <img
                  src='/static/images/light/logo.svg'
                  width='138'
                  height='31'
                  alt='Before You Say Yes'
                />
              </a>
            </Link>
          </div>
          <NavbarToggler
            onClick={() => setCollapsed(!collapsed)}
            className='navbar-toggler-right'
          >
            <i className='fa fa-bars'></i>
          </NavbarToggler>
          <Collapse isOpen={collapsed} navbar>
            <Nav navbar className='mr-auto'>
              {PUBLIC_MENUS.map((item) => {
                return (
                  <NavItem key={item.title}>
                    <ActiveLink
                      activeClassName='active'
                      href={item.link}
                      passHref
                    >
                      <NavLink>{item.title}</NavLink>
                    </ActiveLink>
                  </NavItem>
                );
              })}
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </header>
  );
};

export default Header;
