import useDarkMode from "use-dark-mode";

const Logo = (props) => {
  const { value: isDarkMode } = useDarkMode();
  const dirName = isDarkMode ? "dark" : "light";

  const imageUrl = `/static/images/${dirName}/logo.svg`;

  return <img alt='Before You Say Yes' src={imageUrl} {...props} />;
};

export default Logo;
