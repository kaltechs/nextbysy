import { makeStyles } from "@material-ui/core";
import dynamic from "next/dynamic";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .ql-toolbar": {
      borderLeft: "none",
      borderTop: "none",
      borderRight: "none",
      borderBottom: `1px solid ${theme.palette.divider}`,
      "& .ql-picker-label:hover": {
        color: theme.palette.primary.main,
      },
      "& .ql-picker-label.ql-active": {
        color: theme.palette.primary.main,
      },
      "& .ql-picker-item:hover": {
        color: theme.palette.primary.main,
      },
      "& .ql-picker-item.ql-selected": {
        color: theme.palette.primary.main,
      },
      "& button:hover": {
        color: theme.palette.primary.main,
        "& .ql-stroke": {
          stroke: theme.palette.primary.main,
        },
      },
      "& button:focus": {
        color: theme.palette.primary.main,
        "& .ql-stroke": {
          stroke: theme.palette.primary.main,
        },
      },
      "& button.ql-active": {
        "& .ql-stroke": {
          stroke: theme.palette.primary.main,
        },
      },
      "& .ql-stroke": {
        stroke: theme.palette.text.primary,
      },
      "& .ql-picker": {
        color: theme.palette.text.primary,
      },
      "& .ql-picker-options": {
        padding: theme.spacing(2),
        backgroundColor: theme.palette.background.default,
        border: "none",
        boxShadow: theme.shadows[10],
        borderRadius: theme.shape.borderRadius,
      },
    },
    "& .ql-container": {
      border: "none",
      "& .ql-editor": {
        fontFamily: theme.typography.fontFamily,
        fontSize: 16,
        color: theme.palette.text.primary,
        "&.ql-blank::before": {
          color: theme.palette.text.secondary,
        },
      },
    },
  },
}));

const QuillNoSSRWrapper = dynamic(import("react-quill"), {
  ssr: false,
  loading: () => <p>Loading ...</p>,
});

const modules = {
  toolbar: [
    ["bold", "italic", "underline", "blockquote"],
  ],
  clipboard: {
    // toggle to add extra line breaks when pasting HTML:
    matchVisual: false,
  },
};
/*
 * Quill editor formats
 * See https://quilljs.com/docs/formats/
 */
const formats = [
  "bold",
  "italic",
  "underline",
  "blockquote",
];

export default function SimpleEditor({...rest}) {
  const classes = useStyles();
  return (
    <QuillNoSSRWrapper
      className={classes.root}
      modules={modules}
      formats={formats}
      theme='snow'
      {...rest}
    />
  );
}
