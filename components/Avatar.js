import { Avatar as MaterialAvatar } from "@material-ui/core";
import PropTypes from "prop-types";
import { useState } from "react";
import useDarkMode from "use-dark-mode";

const Avatar = ({ avatar, ...rest }) => {
  const { value: isDarkMode } = useDarkMode();
  const dirName = isDarkMode ? 'dark' : 'light';

  const [avatarUrl, setAvatarUrl] = useState(
    avatar ? avatar : `/static/images/${dirName}/avatar.jpg`
  );

  return <MaterialAvatar src={avatarUrl} {...rest} />;
};

Avatar.propTypes = {
  avatar: PropTypes.string,
};

export default Avatar;
