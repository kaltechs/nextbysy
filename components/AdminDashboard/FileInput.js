import { Controller } from "react-hook-form";
import DropZone from "react-dropzone";
import {
  Box,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Button,
} from "@material-ui/core";
import { CloudUpload, InsertDriveFile } from "@material-ui/icons";
import styles from "./fileInput.module.css";

export const FileInput = ({
  control,
  name,
  buttonAction,
  withUploadButton = false,
}) => {
  console.log(withUploadButton);
  return (
    <Controller
      control={control}
      name={name}
      defaultValue={[]}
      render={({ onChange, onBlur, value }) => {
        return (
          <>
            <DropZone onDrop={onChange} maxFiles={1} preventDropOnDocument>
              {({ getRootProps, getInputProps }) => (
                <Paper
                  variant='outlined'
                  {...getRootProps()}
                  className={styles.root}
                >
                  <CloudUpload className={styles.icon} />
                  <input {...getInputProps()} name={name} onBlur={onBlur} />
                </Paper>
              )}
            </DropZone>
            {value.length > 0 && (
              <>
                <List>
                  {value?.map((f, index) => {
                    return (
                      <ListItem key={index}>
                        <ListItemIcon>
                          <InsertDriveFile />
                        </ListItemIcon>
                        <ListItemText primary={f.name} secondary={f.size} />
                      </ListItem>
                    );
                  })}
                </List>
                {withUploadButton && (
                  <Box mt={2} textAlign='right'>
                    <Button
                      color='primary'
                      variant='contained'
                      onClick={() => {
                        buttonAction(value);
                      }}
                    >
                      Upload Image
                    </Button>
                  </Box>
                )}
              </>
            )}
          </>
        );
      }}
    />
  );
};
