import {
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  makeStyles,
  SvgIcon,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
} from "@material-ui/core";
import { useState } from "react";
import classNames from "classnames";
import {
  Edit as EditIcon,
  ArrowRight as ArrowRightIcon,
  Search as SearchIcon,
} from "react-feather";
import PerfectScrollbar from "react-perfect-scrollbar";
import Link from "next/link";

const tabs = [
  {
    value: "all",
    label: "All",
  },
  {
    value: "isAdmin",
    label: "Administrators",
  },
  {
    value: "isClient",
    label: "Clients",
  },
];

const sortOptions = [
  {
    value: "firstName|asc",
    label: "Alphabetic (asc)",
  },
  {
    value: "lastName|desc",
    label: "Alphabetic (desc)",
  },
];

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500,
  },
  bulkOperations: {
    position: "relative",
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: "absolute",
    width: "100%",
    zIndex: 2,
    backgroundColor: theme.palette.background.default,
  },
  bulkAction: {
    marginLeft: theme.spacing(2),
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1),
  },
}));

const UsersListGrid = ({ className, users, ...rest }) => {
  const classes = useStyles();
  const [searchQuery, setSearchQuery] = useState("");
  const [sort, setSort] = useState(sortOptions[0].value);
  const [currentTab, setCurrentTab] = useState("all");
  const [filter, setFilter] = useState({
    isAdmin: null,
    isClient: null,
  });
  const [selectedUser, setSelectedUsers] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);

  const handleTabsChange = (e, value) => {
    setCurrentTab(value);

    const updatedFilters = {
      ...filter,
      isAdmin: null,
      isClient: null,
    };

    if (value !== "all") {
      updatedFilters[value] = true;
    }

    setFilter(updatedFilters);
    setSelectedUsers([]);
  };

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }

    if (b[orderBy] > a[orderBy]) {
      return 1;
    }

    return 0;
  };

  const getComparator = (order, orderBy) => {
    return order === "desc"
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  };

  const handleSearchQueryChange = (e) => {
    setSearchQuery(e.target.value);
  };

  const handleSortChange = (e) => {
    setSort(e.target.value);
  };

  const applyFilters = (users, searchQuery, filters) => {
    const filteredUsersObject = users.filter((user) => {
      let matches = true;

      if (searchQuery) {
        const searchProperties = ["email", "fullName"];
        let containsSearchQuery = false;

        searchProperties.forEach((property) => {
          if (
            user[property].toLowerCase().includes(searchQuery.toLowerCase())
          ) {
            containsSearchQuery = true;
          }
        });

        if (!containsSearchQuery) {
          matches = false;
        }
      }

      Object.keys(filters).forEach((key) => {
        const value = filters[key];

        if (value && user[key] !== value) {
          matches = false;
        }
      });

      return matches;
    });

    return filteredUsersObject;
  };

  const applySort = (users, sort) => {
    const [orderBy, order] = sort.split("|");
    const comparator = getComparator(order, orderBy);
    const stabilizedThis = users.map((el, index) => [el, index]);

    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);

      if (order !== 0) return order;

      return a[1] - b[1];
    });

    return stabilizedThis.map((el) => el[0]);
  };

  const handleSelectOneCustomer = (e, id) => {
    if (!selectedUser.includes(id)) {
      setSelectedUsers([...selectedUser, id]);
    } else {
      const newSelectedUsersList = selectedUser.filter((user) => user !== id);
      setSelectedUsers(newSelectedUsersList);
    }
  };

  const handleSelectAllCustomers = (e) => {
    setSelectedUsers(
      e.target.checked
        ? users.map((user) => {
            return user.id;
          })
        : []
    );
  };

  const applyPagination = (users, page, limit) => {
    return users.slice(page * limit, page * limit + limit);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(parseInt(event.target.value));
  };

  const filteredUsers = applyFilters(users, searchQuery, filter);
  const allUsersSelected = selectedUser.length === users.length;
  const selectedSomeUsers =
    selectedUser.length > 0 && selectedUser.length !== users.length;
  const sortedUsers = applySort(filteredUsers, sort);
  const paginatedUsers = applyPagination(sortedUsers, page, limit);
  const enableBulkOperations = selectedUser.length > 0;
  const oneUserSelected = selectedUser.length == 1;

  return (
    <Card className={classNames(className, classes.root)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons='auto'
        textColor='primary'
        variant='scrollable'
        value={currentTab}
        indicatorColor='primary'
      >
        {tabs.map((tab) => {
          return <Tab key={tab.value} value={tab.value} label={tab.label} />;
        })}
      </Tabs>
      <Divider />
      <Box mt={2} p={2} minHeight={56} display='flex' alignItems='center'>
        <TextField
          InputProps={{
            startAdornment: (
              <InputAdornment position='start'>
                <SvgIcon fontSize='small' color='action'>
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            ),
          }}
          className={classes.queryField}
          label='Search Users'
          placeholder='Search Users'
          variant='outlined'
          value={searchQuery}
          onChange={handleSearchQueryChange}
        />
        <Box flexGrow={1} />
        <TextField
          label='Sort By'
          name='sort'
          select
          SelectProps={{ native: true }}
          variant='outlined'
          value={sort}
          onChange={handleSortChange}
        >
          {sortOptions.map((options) => {
            return (
              <option key={options.value} value={options.value}>
                {options.label}
              </option>
            );
          })}
        </TextField>
      </Box>
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={allUsersSelected}
              indeterminate={selectedSomeUsers}
              onChange={handleSelectAllCustomers}
              color='primary'
            />
            <Button variant='outlined' className={classes.bulkAction}>
              Delete
            </Button>
            {oneUserSelected && (
              <Button variant='outlined' className={classes.bulkAction}>
                Edit
              </Button>
            )}
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell padding='checkbox'>
                <Checkbox
                  checked={allUsersSelected}
                  indeterminate={selectedSomeUsers}
                  color='primary'
                  onChange={handleSelectAllCustomers}
                />
              </TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Phone</TableCell>
              <TableCell>Country</TableCell>
              <TableCell>City</TableCell>
              <TableCell>State</TableCell>
              <TableCell>Creation Date</TableCell>
              <TableCell align='right'>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {paginatedUsers.map((user) => {
              const isUserSelected = selectedUser.includes(user.id);
              const humancreatedAt = new Date(
                user.createdAt
              ).toLocaleDateString("en-US", {
                day: "numeric",
                month: "short",
                year: "numeric",
              });
              return (
                <TableRow hover key={user._id} selected={isUserSelected}>
                  <TableCell padding='checkbox'>
                    <Checkbox
                      color='primary'
                      checked={isUserSelected}
                      onChange={(e) => handleSelectOneCustomer(e, user.id)}
                      value={isUserSelected}
                    />
                  </TableCell>
                  <TableCell>{user.fullName}</TableCell>
                  <TableCell>{user.email}</TableCell>
                  <TableCell>{user.phone}</TableCell>
                  <TableCell>{user.country}</TableCell>
                  <TableCell>{user.city}</TableCell>
                  <TableCell>{user.state}</TableCell>
                  <TableCell>{humancreatedAt}</TableCell>
                  <TableCell align='right'>
                    <Link href={`/admin/users/${user.id}`}>
                      <IconButton>
                        <SvgIcon fontSize='small'>
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>
                    </Link>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </PerfectScrollbar>
      <TablePagination
        component='div'
        count={filteredUsers.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

export default UsersListGrid;
