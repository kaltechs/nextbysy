import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
} from "@material-ui/core";
import useSwr from "swr";
import { fetcher } from "../../../../util/fetcher";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import LoadScreen from "../../../LoadingScreen";
import { useSnackbar } from "notistack";
import { FileInput } from "../../FileInput";

const generalDataSchema = yup.object().shape({
  email: yup
    .string()
    .email("Please provide a valid email address")
    .required("Please provide an email address"),
  name: yup.string(),
  companyName: yup.string(),
  cardContent: yup
    .string()
    .max(122, "Must be at most 122 characters")
    .required("Card Content is required"),
  intro: yup
    .string()
    .max(500, "Must be at most 500 characters")
    .required("Introduction is required"),
  curiosity: yup
    .string()
    .max(500, "Must be at most 500 characters")
    .required("Curiosity is required"),
  category: yup.string().required("Category is required"),
  introVideoUrl: yup.string().url('Please add a valid URL').required('Intro Video URL is required')
});

const GeneralSettings = ({ expoData, setExpoData }) => {
  const { enqueueSnackbar } = useSnackbar();
  const { data: categoryList, error } = useSwr("/api/expo/category", fetcher);

  const { register, handleSubmit, errors, control } = useForm({
    defaultValues: expoData,
    mode: "onBlur",
    resolver: yupResolver(generalDataSchema),
  });

  if (!categoryList && !error) {
    return <LoadScreen />;
  }

  const onSubmit = (data) => {
    setExpoData({
      ...expoData,
      ...data,
    });
    enqueueSnackbar("Settings sent to 'Summary' tab", {
      variant: "info",
    });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid container spacing={3}>
        <Grid item xs={12} lg={4}>
          <Card>
            <CardHeader title='Settings' />
            <Divider />
            <CardContent>
              <TextField
                fullWidth
                label='Email'
                name='email'
                variant='outlined'
                inputRef={register}
                error={!!errors.email}
                helperText={errors?.email?.message}
              />
              <Box mt={2}>
                <TextField
                  fullWidth
                  label='Full Name'
                  name='name'
                  variant='outlined'
                  inputRef={register}
                  error={!!errors.name}
                  helperText={errors?.name?.message}
                />
              </Box>
              <Box mt={2}>
                <TextField
                  fullWidth
                  label='Company Name'
                  name='companyName'
                  variant='outlined'
                  inputRef={register}
                  error={!!errors.companyName}
                  helperText={errors?.companyName?.message}
                />
              </Box>
              <Box mt={2}>
                <TextField
                  fullWidth
                  label='Category'
                  name='category'
                  select
                  SelectProps={{ native: true }}
                  variant='outlined'
                  inputRef={register}
                >
                  {categoryList.map((category) => {
                    return (
                      <option value={category.slug} key={category.slug}>
                        {category.name}
                      </option>
                    );
                  })}
                </TextField>
              </Box>
              <Box mt={2}>
                <FileInput control={control} name='avatar' />
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} lg={8}>
          <Card>
            <CardContent>
              <Box mb={2}>
                <TextField
                  multiline
                  rows={5}
                  fullWidth
                  inputRef={register}
                  name='cardContent'
                  error={!!errors.cardContent}
                  helperText={errors?.cardContent?.message}
                  variant='outlined'
                  label='Card Content'
                />
              </Box>
              <Box mt={3} mb={2}>
                <TextField
                  multiline
                  rows={5}
                  fullWidth
                  inputRef={register}
                  name='intro'
                  error={!!errors.intro}
                  helperText={errors?.intro?.message}
                  variant='outlined'
                  label='Intro'
                />
              </Box>
              <Box mb={2}>
                <TextField
                  multiline
                  rows={5}
                  fullWidth
                  inputRef={register}
                  name='curiosity'
                  error={!!errors.curiosity}
                  helperText={errors?.curiosity?.message}
                  variant='outlined'
                  label='Curiosity'
                />
              </Box>
              <Box mb={2}>
                <TextField
                  fullWidth
                  inputRef={register}
                  name='introVideoUrl'
                  error={!!errors.introVideoUrl}
                  helperText={errors?.introVideoUrl?.message}
                  variant='outlined'
                  label='Video Intro URL' />
              </Box>
            </CardContent>
            <Divider />
            <Box p={2} display='flex' justifyContent='flex-end'>
              <Button color='primary' variant='contained' type='submit'>
                Save Changes
              </Button>
            </Box>
          </Card>
        </Grid>
      </Grid>
    </form>
  );
};

export default GeneralSettings;
