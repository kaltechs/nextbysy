import {
  Card,
  CardContent,
  Grid,
  TextField,
  Box,
  Button,
  CardHeader,
  Divider,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { useFieldArray } from "react-hook-form";

const HeadQuarterForm = ({ register, control }) => {
  const { fields, append, remove, insert } = useFieldArray({
    control,
    name: "headquarters",
  });

  return (
    <Card>
      <CardHeader title='HeadQuarters' />
      <Divider />
      <CardContent>
        {fields.map((item, index) => {
          return (
            <Box mb={4} mt={5} key={item.id}>
              <Grid
                container
                item
                xs={12}
                spacing={5}
                alignContent='center'
                justify='center'
                alignItems='center'
              >
                <Grid item xs={12} md={4} lg={4}>
                  <Box
                    mb={2}
                    display='flex'
                    alignSelf='center'
                    alignContent='center'
                    alignItems='center'
                  >
                    <TextField
                      fullWidth
                      label='HeadQuarter'
                      name={`headquarters[${index}].name`}
                      variant='outlined'
                      defaultValue={item.name}
                      inputRef={register()}
                    />
                  </Box>
                </Grid>
                <Grid item xs={12} md={4} lg={4}>
                  <Box
                    mb={2}
                    display='flex'
                    alignSelf='center'
                    alignContent='center'
                    alignItems='center'
                  >
                    <TextField
                      name={`headquarters[${index}].value`}
                      label='Address'
                      fullWidth
                      inputRef={register()}
                      variant='outlined'
                      defaultValue={item.value}
                    />
                  </Box>
                </Grid>
                <Grid item xs={12} md={4}>
                  <Box display='flex' justifyContent='center'>
                    <Button
                      color='primary'
                      variant='contained'
                      onClick={() => remove(index)}
                      startIcon={<DeleteIcon />}
                    >
                      Remove
                    </Button>
                  </Box>
                </Grid>
              </Grid>
              <Divider />
            </Box>
          );
        })}
        <Box mt={3}>
          <Button
            color='primary'
            variant='contained'
            onClick={() => append({ name: "", value: "" })}
          >
            Add New HQ
          </Button>
        </Box>
      </CardContent>
    </Card>
  );
};

export default HeadQuarterForm;
