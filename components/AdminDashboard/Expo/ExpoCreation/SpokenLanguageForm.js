import { LANGUAGES } from "../../../../util/enums";
import {
  Card,
  CardContent,
  Grid,
  Slider,
  TextField,
  Box,
  Button,
  CardHeader,
  Divider,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { useFieldArray, Controller } from "react-hook-form";

const SpokenLanguageForm = ({ register, control }) => {
  const { fields, append, remove, insert } = useFieldArray({
    control,
    name: "lang",
  });

  const valuetext = (value) => {
    return `${value}`;
  };

  return (
    <Card>
      <CardHeader title='Spoken Languages' />
      <Divider />
      <CardContent>
        {fields.map((item, index) => {
          return (
            <Box mb={4} mt={5} key={item.id}>
              <Grid
                container
                item
                xs={12}
                spacing={5}
                alignContent='center'
                justify='center'
                alignItems='center'
              >
                <Grid item xs={12} md={4} lg={4}>
                  <Box
                    mb={2}
                    display='flex'
                    alignSelf='center'
                    alignContent='center'
                    alignItems='center'
                  >
                    <TextField
                      fullWidth
                      label='Language'
                      name={`lang[${index}].name`}
                      defaultValue={item.name}
                      select
                      SelectProps={{ native: true }}
                      variant='outlined'
                      inputRef={register()}
                    >
                      {LANGUAGES.map((lang) => {
                        return (
                          <option value={lang.name} key={lang.name}>
                            {lang.name}
                          </option>
                        );
                      })}
                    </TextField>
                  </Box>
                </Grid>
                <Grid item xs={12} md={4} lg={4}>
                  <Box
                    mb={2}
                    display='flex'
                    alignSelf='center'
                    alignContent='center'
                    alignItems='center'
                  >
                    <Controller
                      name={`lang[${index}].value`}
                      control={control}
                      defaultValue={item.value}
                      render={(props) => (
                        <Slider
                          {...props}
                          onChange={(_, value) => {
                            props.onChange(value);
                          }}
                          defaultValue={1}
                          steps={1}
                          marks
                          min={1}
                          max={10}
                          getAriaValueText={valuetext}
                          name={`lang[${index}].value`}
                          valueLabelDisplay='on'
                        />
                      )}
                    />
                  </Box>
                </Grid>
                <Grid item xs={12} md={4}>
                  <Box display='flex' justifyContent='center'>
                    <Button
                      color='primary'
                      variant='contained'
                      onClick={() => remove(index)}
                      startIcon={<DeleteIcon />}
                    >
                      Remove
                    </Button>
                  </Box>
                </Grid>
              </Grid>
              <Divider />
            </Box>
          );
        })}
        <Box mt={3}>
          <Button
            color='primary'
            variant='contained'
            onClick={() => append({ name: "", value: 0 })}
          >
            Add New Language
          </Button>
        </Box>
      </CardContent>
    </Card>
  );
};

export default SpokenLanguageForm;
