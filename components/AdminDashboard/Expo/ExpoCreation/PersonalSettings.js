import { Box, Button } from "@material-ui/core";
import { useForm } from "react-hook-form";
import SpokenLanguageForm from "./SpokenLanguageForm";
import HeadQuarterForm from "./HeadQuartersForm";
import PricesForm from "./PricesForm";
import { useSnackbar } from "notistack";

const PersonalSettings = ({ personalData, setPersonalData }) => {
  const { enqueueSnackbar } = useSnackbar();

  const submitPersonalForm = (data) => {
    setPersonalData({
      ...personalData,
      spokenLanguages: data.lang,
      headquarters: data.headquarters,
      prices: data.price,
    });

    enqueueSnackbar("Settings sent to 'Summary' tab", {
      variant: "info",
    });
  };

  const { register, handleSubmit, setError, control } = useForm({
    defaultValues: {
      lang: personalData.spokenLanguages,
      headquarters: personalData.headquarters,
      price: personalData.prices,
    },
  });

  return (
    <>
      <form onSubmit={handleSubmit(submitPersonalForm)}>
        <Box p={2} display='flex' justifyContent='flex-end'>
          <Button color='primary' variant='contained' type='submit'>
            Save personal settings
          </Button>
        </Box>
        <SpokenLanguageForm register={register} control={control} />
        <Box mt={2} />
        <HeadQuarterForm register={register} control={control} />
        <Box mt={2} />
        <PricesForm register={register} control={control} />
        <Box p={2} display='flex' justifyContent='flex-end'>
          <Button color='primary' variant='contained' type='submit'>
            Save personal settings
          </Button>
        </Box>
      </form>
    </>
  );
};

export default PersonalSettings;
