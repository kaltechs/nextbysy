import {
  Card,
  CardContent,
  CardHeader,
  Box,
  Typography,
  Divider,
  Grid,
  Switch,
  Button,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import { InsertDriveFile } from "@material-ui/icons";
import axios from "axios";
import { useState } from "react";
import { useSnackbar } from "notistack";

const ExpoCreationResult = ({ expoData, setExpoData }) => {
  let title = expoData.name;

  const [pending, setPending] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  if (expoData.name !== "") {
    title = expoData.name;
  }

  if (expoData.companyName !== "") {
    title += ` - ${expoData.companyName}`;
  }

  const handleDraftSwitchChange = (e) => {
    setExpoData({
      ...expoData,
      isDraft: e.target.checked,
      isPublished: !e.target.checked,
    });
  };

  const handlePublishSwitchChange = (e) => {
    setExpoData({
      ...expoData,
      isPublished: e.target.checked,
      isDraft: !e.target.checked,
    });
  };

  const handleSubmitExpoItem = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    setPending(true);

    const entries = Object.entries(expoData).filter(
      (entry) => entry[0] !== "avatar"
    );

    entries.forEach((entry) => {
      if (
        entry[0] === "spokenLanguages" ||
        entry[0] === "headquarters" ||
        entry[0] === "prices"
      ) {
        formData.append(entry[0], JSON.stringify(entry[1]));
      } else {
        formData.append(entry[0], entry[1]);
      }
    });

    if (expoData.avatar) {
      expoData.avatar.forEach((file) => {
        formData.append("avatar", file);
      });
    }

    const config = {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    await axios
      .post("/api/expo", formData, config)
      .then((resp) => {
        console.log(resp);
        setPending(false);
        enqueueSnackbar("Expo saved", {
          variant: "success",
        });
      })
      .catch((err) => {
        console.log(err);
        setPending(false);
        enqueueSnackbar(err.response.data.message, {
          variant: "error",
        });
      });
  };

  return (
    <Card>
      <CardHeader title={title} />
      <Divider />
      <CardContent>
        <Grid container>
          <Grid item xs={12} md={4}>
            <Typography gutterBottom variant='h6' color='textPrimary'>
              Name
            </Typography>
            <Typography gutterBottom variant='body2' color='textSecondary'>
              {expoData.name}
            </Typography>
          </Grid>
          <Grid item xs={12} md={4}>
            <Typography gutterBottom variant='h6' color='textPrimary'>
              Company
            </Typography>
            <Typography gutterBottom variant='body2' color='textSecondary'>
              {expoData.companyName}
            </Typography>
          </Grid>
          <Grid item xs={12} md={4}>
            <Typography gutterBottom variant='h6' color='textPrimary'>
              Email
            </Typography>
            <Typography gutterBottom variant='body2' color='textSecondary'>
              {expoData.email}
            </Typography>
          </Grid>
        </Grid>

        <Box mb={3} mt={3}>
          <Typography gutterBottom variant='h6' color='textPrimary'>
            Card Content
          </Typography>
          <Typography gutterBottom variant='body2' color='textSecondary'>
            {expoData.cardContent}
          </Typography>
        </Box>

        <Box mb={3}>
          <Typography gutterBottom variant='h6' color='textPrimary'>
            Introduction
          </Typography>
          <Typography gutterBottom variant='body2' color='textSecondary'>
            {expoData.intro}
          </Typography>
        </Box>

        <Box mb={3}>
          <Typography gutterBottom variant='h6' color='textPrimary'>
            Curiosity
          </Typography>
          <Typography gutterBottom variant='body2' color='textSecondary'>
            {expoData.curiosity}
          </Typography>
        </Box>

        <Grid container>
          <Grid item xs={12} md={4}>
            <Box mb={3}>
              <Typography gutterBottom variant='h6' color='textPrimary'>
                Spoken Languages
              </Typography>
              {expoData.spokenLanguages.map((item, index) => {
                return (
                  <Typography
                    gutterBottom
                    variant='body2'
                    color='textSecondary'
                    key={`lang_${index}`}
                  >{`${item.name} - ${item.value}/10`}</Typography>
                );
              })}
            </Box>
          </Grid>
          <Grid item xs={12} md={4}>
            <Box mb={3}>
              <Typography gutterBottom variant='h6' color='textPrimary'>
                Headquarters
              </Typography>
              {expoData.headquarters.map((item, index) => {
                return (
                  <Typography
                    gutterBottom
                    variant='body2'
                    color='textSecondary'
                    key={`head_${index}`}
                  >{`${item.name} - ${item.value}`}</Typography>
                );
              })}
            </Box>
          </Grid>
          <Grid item xs={12} md={4}>
            <Box mb={3}>
              <Typography gutterBottom variant='h6' color='textPrimary'>
                Prices
              </Typography>
              {expoData.prices?.map((item, index) => {
                return (
                  <Typography
                    gutterBottom
                    variant='body2'
                    color='textSecondary'
                    key={`head_${index}`}
                  >{`${item.name}: ${item.value}`}</Typography>
                );
              })}
            </Box>
          </Grid>
          <Grid item xs={12} md={4}>
            <Box mb={3}>
              <Typography gutterBottom variant='h6' color='textPrimary'>
                Expo Image
              </Typography>
              <List>
                {expoData.avatar?.map((f, index) => (
                  <ListItem key={index}>
                    <ListItemIcon>
                      <InsertDriveFile />
                    </ListItemIcon>
                    <ListItemText primary={f.name} secondary={f.size} />
                  </ListItem>
                ))}
              </List>
            </Box>
          </Grid>
        </Grid>
      </CardContent>
      <Divider />
      <Box p={2}>
        <Typography variant='h6' color='textPrimary'>
          Publish / Draft
        </Typography>
        <Typography variant='body2' color='textSecondary'>
          When saving in publish mode, the Expo person will be available for the
          public.
        </Typography>
        Save as Draft{" "}
        <Switch
          checked={expoData.isDraft}
          edge='start'
          name='isDraft'
          onChange={handleDraftSwitchChange}
          color='primary'
        />
        Save as Published
        <Switch
          checked={expoData.isPublished}
          edge='start'
          name='isPublished'
          onChange={handlePublishSwitchChange}
          color='primary'
        />
      </Box>
      <Box p={2} display='flex' justifyContent='flex-end'>
        <Button
          color='primary'
          variant='contained'
          onClick={handleSubmitExpoItem}
          disabled={pending}
        >
          Save
        </Button>
      </Box>
    </Card>
  );
};

export default ExpoCreationResult;
