import useSwr from "swr";
import { fetcher } from "../../../../util/fetcher";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { FileInput } from "../../FileInput";
import LoadingScreen from "../../../LoadingScreen";
import Image from "next/image";

import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  Box,
  Button,
  Typography,
  Switch,
  IconButton,
  Tooltip,
} from "@material-ui/core";
import axios from "axios";
import { useState } from "react";
import { Clear } from "@material-ui/icons";
import classes from "./generalSettings.module.css";

const generalDataSchema = yup.object().shape({
  name: yup.string(),
  companyName: yup.string(),
  cardContent: yup
    .string()
    .max(122, "Must be at most 122 characters")
    .required("Card Content is required"),
  intro: yup
    .string()
    .max(500, "Must be at most 500 characters")
    .required("Introduction is required"),
  curiosity: yup
    .string()
    .max(500, "Must be at most 500 characters")
    .required("Curiosity is required"),
  category: yup.string().required("Category is required"),
  introVideoUrl: yup
    .string()
    .url("Please add a valid URL")
    .required("Intro Video URL is required"),
});

const GeneralSettings = ({
  expoData,
  setExpoData,
  onSubmit,
  pending,
  onAvatarDelete,
  onAvatarSubmit
}) => {
  const { data: categoryList, error } = useSwr("/api/expo/category", fetcher);

  const { register, handleSubmit, errors, control } = useForm({
    defaultValues: expoData,
    mode: "onBlur",
    resolver: yupResolver(generalDataSchema),
  });

  const handleDraftSwitchChange = (e) => {
    setExpoData({
      ...expoData,
      isDraft: e.target.checked,
      isPublished: !e.target.checked,
    });
  };

  const handlePublishSwitchChange = (e) => {
    setExpoData({
      ...expoData,
      isPublished: e.target.checked,
      isDraft: !e.target.checked,
    });
  };

  const onGeneralFormSubmit = (data) => {
    setExpoData({
      ...expoData,
      ...data,
      avatar: data.avatar,
    });

    onSubmit(true);
  };

  if (!categoryList && !error) {
    return <LoadingScreen />;
  }

  return (
    <form onSubmit={handleSubmit(onGeneralFormSubmit)}>
      <Grid container spacing={3}>
        <Grid item xs={12} lg={4}>
          <Card>
            <CardHeader title='General Settings' />
            <Divider />
            <CardContent>
              <TextField
                fullWidth
                label='Email'
                name='email'
                variant='outlined'
                disabled
                value={expoData.email}
              />
              <Box mt={2}>
                <TextField
                  fullWidth
                  label='Full Name'
                  name='name'
                  variant='outlined'
                  inputRef={register}
                  error={!!errors.name}
                  helperText={errors?.name?.message}
                />
              </Box>
              <Box mt={2}>
                <TextField
                  fullWidth
                  label='Company Name'
                  name='companyName'
                  variant='outlined'
                  inputRef={register}
                  error={!!errors.companyName}
                  helperText={errors?.companyName?.message}
                />
              </Box>
              <Box mt={2}>
                <TextField
                  fullWidth
                  label='Category'
                  name='category'
                  select
                  SelectProps={{ native: true }}
                  variant='outlined'
                  inputRef={register}
                >
                  {categoryList.map((category) => {
                    return (
                      <option value={category.slug} key={category.slug}>
                        {category.name}
                      </option>
                    );
                  })}
                </TextField>
              </Box>
              <Box mt={2}>
                {!expoData.expoimg ? (
                  <FileInput control={control} name='avatar' withUploadButton buttonAction={onAvatarSubmit} />
                ) : (
                  <div className={classes.image_container}>
                    <Tooltip title='Delete Image'>
                      <IconButton
                        color='primary'
                        className={classes.btnRemoveImage}
                        onClick={onAvatarDelete}
                      >
                        <Clear />
                      </IconButton>
                    </Tooltip>
                    <Image
                      src={expoData.expoimg}
                      width={50}
                      height={96}
                      layout='responsive'
                    />
                  </div>
                )}
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} lg={8}>
          <Card>
            <CardContent>
              <Box mb={2}>
                <TextField
                  multiline
                  rows={5}
                  fullWidth
                  inputRef={register}
                  name='cardContent'
                  error={!!errors.cardContent}
                  helperText={errors?.cardContent?.message}
                  variant='outlined'
                  label='Card Content'
                />
              </Box>
              <Box mt={3} mb={2}>
                <TextField
                  multiline
                  rows={5}
                  fullWidth
                  inputRef={register}
                  name='intro'
                  error={!!errors.intro}
                  helperText={errors?.intro?.message}
                  variant='outlined'
                  label='Intro'
                />
              </Box>
              <Box mb={2}>
                <TextField
                  multiline
                  rows={5}
                  fullWidth
                  inputRef={register}
                  name='curiosity'
                  error={!!errors.curiosity}
                  helperText={errors?.curiosity?.message}
                  variant='outlined'
                  label='Curiosity'
                />
              </Box>
              <Box mb={2}>
                <TextField
                  fullWidth
                  inputRef={register}
                  name='introVideoUrl'
                  error={!!errors.introVideoUrl}
                  helperText={errors?.introVideoUrl?.message}
                  variant='outlined'
                  label='Video Intro URL'
                />
              </Box>
            </CardContent>
            <Divider />
            <Box p={2}>
              <Typography variant='h6' color='textPrimary'>
                Publish / Draft
              </Typography>
              <Typography variant='body2' color='textSecondary'>
                When saving in publish mode, the Expo person will be available
                for the public.
              </Typography>
              Save as Draft{" "}
              <Switch
                checked={expoData.isDraft}
                edge='start'
                name='isDraft'
                onChange={handleDraftSwitchChange}
                color='primary'
              />
              Save as Published
              <Switch
                checked={expoData.isPublished}
                edge='start'
                name='isPublished'
                onChange={handlePublishSwitchChange}
                color='primary'
              />
            </Box>
            <Box p={2} display='flex' justifyContent='flex-end'>
              <Button
                color='primary'
                variant='contained'
                type='submit'
                disabled={pending}
              >
                Update Changes
              </Button>
            </Box>
          </Card>
        </Grid>
      </Grid>
    </form>
  );
};

export default GeneralSettings;
