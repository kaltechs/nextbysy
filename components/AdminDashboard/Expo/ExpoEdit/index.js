import { Box, Divider, Tab, Tabs } from "@material-ui/core";
import { useEffect, useState } from "react";
import dynamic from "next/dynamic";
import axios from "axios";
import { useSnackbar } from "notistack";
import GeneralSettings from "./GeneralSettings";
import PersonalSettings from "./PersonalSettings";

const tabs = [
  { value: "general", label: "General" },
  { value: "personal", label: "Personal" },
  { value: "result", label: "Summary" },
];

// const GeneralSettings = dynamic(() => import("./GeneralSettings"), {
//   ssr: false,
// });

// const PersonalSettings = dynamic(() => import("./PersonalSettings"), {
//   ssr: false,
// });

const EditTabs = ({ data }) => {
  const [currentTab, setCurrentTab] = useState("general");
  const [expoFormData, setFormData] = useState(data);
  const [pending, setPending] = useState(false);
  const [toUpdate, setToUpdate] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    if (toUpdate) {
      onSubmit();
    }
  }, [toUpdate]);

  const handleTabChange = (e, value) => {
    setCurrentTab(value);
  };

  const onAvatarDelete = async () => {
    setPending(true);
    const deleteValue = {
      _id: expoFormData._id,
    };

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    await axios
      .patch(`/api/expo/avatar`, deleteValue, config)
      .then((resp) => {
        enqueueSnackbar("Image removed", {
          variant: "success",
        });
        setPending(false);

        setFormData(resp.data);
      })
      .catch((err) => {
        setPending(false);
        enqueueSnackbar(err.response.data.message, {
          variant: "error",
        });
      });
  };

  const onAvatarSubmit = async (files) => {
    const avatarFormData = new FormData();

    avatarFormData.append("email", expoFormData.email);

    if (files) {
      files.forEach((file) => {
        avatarFormData.append("avatar", file);
      });
    }

    const addAvatarConfig = {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    await axios
      .post("/api/expo/addAvatar", avatarFormData, addAvatarConfig)
      .then((resp) => {
        setFormData({
          ...expoFormData,
          expoimg: resp.data.message,
        });

        enqueueSnackbar("Image updated", {
          variant: "success",
        });
      })
      .catch((err) => {
        console.log(err);
        setPending(false);
        enqueueSnackbar(err.response.data.message, {
          variant: "error",
        });
      });
  };

  const onSubmit = async () => {
    setPending(true);

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    await axios
      .patch(`/api/expo/${expoFormData._id}`, expoFormData, config)
      .then((resp) => {
        enqueueSnackbar("Expo updated", {
          variant: "success",
        });
        setPending(false);
      })
      .catch((err) => {
        setPending(false);
        enqueueSnackbar(err.response.data.message, {
          variant: "error",
        });
      });
  };

  return (
    <>
      <Box mt={3}>
        <Tabs
          onChange={handleTabChange}
          scrollButtons='auto'
          value={currentTab}
          variant='scrollable'
          textColor='primary'
          indicatorColor='primary'
        >
          {tabs.map((tab) => (
            <Tab key={tab.value} label={tab.label} value={tab.value} />
          ))}
        </Tabs>
      </Box>
      <Divider />
      <Box mt={3}>
        {currentTab === "general" && (
          <GeneralSettings
            expoData={expoFormData}
            setExpoData={setFormData}
            onSubmit={setToUpdate}
            pending={pending}
            onAvatarDelete={onAvatarDelete}
            onAvatarSubmit={onAvatarSubmit}
          />
        )}
        {currentTab === "personal" && (
          <PersonalSettings
            expoData={expoFormData}
            setExpoData={setFormData}
            onSubmit={setToUpdate}
            pending={pending}
          />
        )}
      </Box>
    </>
  );
};

export default EditTabs;
