import { useForm } from "react-hook-form";
import dynamic from "next/dynamic";
import { Box, Button } from "@material-ui/core";

const SpokenLanguageForm = dynamic(() =>
  import("../ExpoCreation/SpokenLanguageForm")
);
const HeadQuarterForm = dynamic(() =>
  import("../ExpoCreation/HeadQuartersForm")
);
const PricesForm = dynamic(() => import("../ExpoCreation/PricesForm"));

const PersonalSettings = ({ expoData, setExpoData, onSubmit, pending }) => {
  const { register, handleSubmit, setError, control } = useForm({
    defaultValues: {
      lang: expoData.spokenLanguages
        ? expoData.spokenLanguages
        : [{ name: "", value: 0 }],
      headquarters: expoData.headquarters
        ? expoData.headquarters
        : [{ name: "", value: "" }],
      price: expoData.prices ? expoData.prices : [{ name: "", value: "" }],
    },
  });

  const submitPersonalForm = async (data) => {
    const toUpdate = {
      spokenLanguages: data.lang ? data.lang : [],
      headquarters: data.headquarters ? data.headquarters : [],
      prices: data.price ? data.price : [],
    };

    setExpoData({
      ...expoData,
      ...toUpdate,
      avatar: [],
    });

    onSubmit(true);
  };

  return (
    <>
      <form onSubmit={handleSubmit(submitPersonalForm)}>
        <Box p={2} display='flex' justifyContent='flex-end'>
          <Button
            color='primary'
            variant='contained'
            type='submit'
            disabled={pending}
          >
            Update personal settings
          </Button>
        </Box>
        <SpokenLanguageForm register={register} control={control} />
        <Box mt={2} />
        <HeadQuarterForm register={register} control={control} />
        <Box mt={2} />
        <PricesForm register={register} control={control} />
        <Box p={2} display='flex' justifyContent='flex-end'>
          <Button
            color='primary'
            variant='contained'
            type='submit'
            disabled={pending}
          >
            Update personal settings
          </Button>
        </Box>
      </form>
    </>
  );
};

export default PersonalSettings;
