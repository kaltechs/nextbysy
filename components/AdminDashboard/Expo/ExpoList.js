import { Button, Card, CardContent, Grid } from "@material-ui/core";
import { DataGrid } from "@material-ui/data-grid";
import { useRouter } from "next/router";

const ExpoList = ({ expoList }) => {
  const router = useRouter();

  const columns = [
    { field: "_id", headerName: "Id", type: "string", hide: true },
    { field: "name", headerName: "Full Name", type: "string", flex: 1 },
    { field: "companyName", headerName: "Company", type: "string", flex: 1 },
    { field: "email", headerName: "Email", type: "string", flex: 1 },
    { field: "category", headerName: "Category", type: "string", flex: 1 },
    { field: "isDraft", headerName: "Draft", type: "boolean", flex: 1 },
    { field: "isPublished", headerName: "Live", type: "boolean", flex: 1 },
    {
      field: "id",
      headerName: "Actions",
      flex: 1,
      renderCell: (params) => {
        return (
          <strong>
            <Button
              color='primary'
              variant='contained'
              value={params.row.id}
              onClick={handleEditClick}
            >
              Edit/View
            </Button>
          </strong>
        );
      },
    },
  ];

  const handleEditClick = (e) => {
    e.preventDefault();
    router.push(`expo/${e.currentTarget.value}`);
  };

  return (
    <>
      <Card>
        <CardContent>
          <Grid container>
            <DataGrid
              autoHeight
              rows={expoList}
              columns={columns}
              pageSize={10}
              rowsPerPageOptions={[5, 10, 20, 50, 100]}
            />
          </Grid>
        </CardContent>
      </Card>
    </>
  );
};

export default ExpoList;
