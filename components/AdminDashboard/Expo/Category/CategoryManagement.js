import { useEffect, useState } from "react";
import { useSnackbar } from "notistack";
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  Button,
  Box,
  FormHelperText,
  makeStyles,
} from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import slugify from "slugify";
import PropTypes from "prop-types";
import { trigger } from "swr";
import { TwitterPicker } from "react-color";

const useStyle = makeStyles((theme) => ({
  picker: {
    backgroundColor: theme.palette.background.default + ' !important',
    '& input': {
      height: '30px !important'
    }
  },
}));

const CategoryManagement = ({
  className,
  categories,
  toEditData,
  setEditData,
  ...rest
}) => {
  const classes = useStyle();
  const [pending, setPending] = useState(false);
  const { enqueueSnackbar } = useSnackbar();
  const [data, setData] = useState(toEditData);
  const [background, setBackground] = useState(data.color);

  useEffect(() => {
    if (toEditData && toEditData._id !== null) {
      setData(toEditData);
      setBackground(toEditData.color);
    }
  }, [toEditData]);

  return (
    <Formik
      enableReinitialize
      initialValues={data}
      validationSchema={Yup.object().shape({
        name: Yup.string().required("Category name is required"),
        slug: Yup.string().required("Slug is required"),
      })}
      onSubmit={async (
        values,
        { resetForm, setErrors, setStatus, setSubmitting }
      ) => {
        try {
          setPending(true);
          const config = {
            headers: {
              "Content-Type": "application/json",
            },
          };

          console.log(values);

          await axios
            .post("/api/expo/category", values, config)
            .then((resp) => {
              trigger("/api/expo/category");
              setEditData({ _id: "", name: "", slug: "", color: "#FFF" });
              setPending(false);
              setSubmitting(false);
              setStatus({ success: true });
              resetForm();
              enqueueSnackbar("Category saved", {
                variant: "success",
              });
            });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.response.data.message });
          setSubmitting(false);
          setPending(false);
          enqueueSnackbar("Error occured", {
            variant: "error",
          });
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
        setFieldValue,
      }) => (
        <form onSubmit={handleSubmit}>
          <Card className={className} {...rest}>
            <CardHeader
              title='Expo category'
              style={{
                backgroundColor: background !== "#FFF" ? background : "inherit",
              }}
            />
            <Divider />
            <CardContent>
              <Grid container spacing={4}>
                <Grid item xs={12}>
                  <TextField
                    error={Boolean(touched.name && errors.name)}
                    helperText={touched.name && errors.name}
                    name='name'
                    label='Category name'
                    onBlur={handleBlur}
                    onChange={(e) => {
                      const slug = slugify(e.target.value, {
                        replacement: "_",
                        remove: /[*+~.()'"!:@]/g,
                        lower: true,
                      });
                      handleChange(e);
                      setFieldValue("slug", slug);
                    }}
                    value={values.name}
                    variant='outlined'
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    error={Boolean(touched.slug && errors.slug)}
                    helperText={touched.slug && errors.slug}
                    name='slug'
                    label='Category slug'
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.slug}
                    variant='outlined'
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid item xs={12}>
                  <TwitterPicker
                    width='284px'
                    color={background}
                    className={classes.picker}
                    onChangeComplete={(e) => {
                      setFieldValue("color", e.hex);
                      setBackground(e.hex);
                    }}
                  />
                </Grid>
              </Grid>
              {errors.submit && (
                <Box
                  mt={2}
                  mb={2}
                  textAlign='center'
                  alignSelf='center'
                  alignContent='center'
                  alignItems='center'
                >
                  <FormHelperText error>{errors.submit}</FormHelperText>
                </Box>
              )}
            </CardContent>
            <Divider />
            <Box p={2} display='flex' justifyContent='flex-end'>
              <Button
                color='primary'
                disabled={isSubmitting}
                variant='contained'
                type='submit'
              >
                Save Changes
              </Button>
            </Box>
          </Card>
        </form>
      )}
    </Formik>
  );
};

CategoryManagement.propTypes = {
  categories: PropTypes.array,
  toEditData: PropTypes.object,
};

export default CategoryManagement;
