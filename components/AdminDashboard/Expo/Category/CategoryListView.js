import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  makeStyles,
} from "@material-ui/core";
import { Fragment, useState } from "react";
import { DataGrid } from "@material-ui/data-grid";
import PropTypes from "prop-types";
import axios from "axios";
import { useSnackbar } from "notistack";
import { mutate, trigger } from "swr";

const useStyle = makeStyles((theme) => {
  return {
    root: {},
    dataGrid: {
      width: "100%",
      height: "400px",
    },
    dataGridContainer: {
      display: "flex",
      height: "100%",
    },
    bulkOperations: {
      position: "relative",
    },
    bulkActions: {
      paddingLeft: 4,
      paddingRight: 4,
      marginTop: 6,
      marginBottom: 15,
      position: "relative",
      width: "100%",
      zIndex: 2,
      backgroundColor: theme.palette.background.default,
    },
    bulkAction: {
      marginLeft: theme.spacing(2),
      "&:nth-of-type(1)": {
        marginLeft: "-4px",
      },
    },
  };
});

const columns = [
  { field: "_id", headerName: "Id", type: "string", hide: true },
  { field: "name", headerName: "Category", type: "string", flex: 1 },
  { field: "slug", headerName: "Slug", type: "string", flex: 1 },
  { field: "color", headerName: "Color", type: "string", flex: 1, renderCell: (params) => {
    return (
      <Box style={{backgroundColor: params.row.color, width: '100%'}} textAlign='center'>
        <p style={{color: 'black'}}>{params.row.color}</p>
      </Box>
    )
  } },
];

const CategoryListView = ({ categories, editData }) => {
  const classes = useStyle();
  const [selectedCategory, setSelectedCategory] = useState([]);
  const { enqueueSnackbar } = useSnackbar();

  const handleOnSelectionChange = (e) => {
    if (e.selectionModel.length) {
      setSelectedCategory(e.selectionModel);
    } else {
      setSelectedCategory([]);
    }
  };

  const handleDeleteClick = async (e) => {
    if (selectedCategory) {
      mutate(
        "/api/expo/category",
        categories.filter((x) => !selectedCategory.includes(x._id)),
        false
      );
      await axios
        .delete(`/api/expo/category/${selectedCategory}`)
        .then((resp) => {
          trigger("/api/expo/category");
          enqueueSnackbar("Category deleted", {
            variant: "success",
          });
          setSelectedCategory([]);
        })
        .catch((error) => {
          trigger("/api/expo/category");
          enqueueSnackbar(error.response.data.message, {
            variant: "error",
          });
        });
    }
  };

  const handleEditClick = (e) => {
    e.preventDefault();
    if (selectedCategory && selectedCategory.length == 1) {
      var categoryModel = categories.filter((cat) => {
        return cat._id === selectedCategory[0];
      });

      if (categoryModel.length) {
        editData(categoryModel[0]);
        setSelectedCategory([]);
      }
    }
  };

  const enableBulkOperations = selectedCategory.length > 0;
  const oneCategorySelected = selectedCategory.length == 1;

  return (
    <Fragment>
      <Card className={classes.root}>
        <CardHeader title='Expo Categories' />
        <Divider />
        <CardContent>
          <Grid container>
            <Grid item md={12} xs={12}>
              {enableBulkOperations && (
                <div className={classes.bulkOperations}>
                  <div className={classes.bulkActions}>
                    <Button
                      variant='outlined'
                      className={classes.bulkAction}
                      onClick={handleDeleteClick}
                    >
                      Delete
                    </Button>
                    {oneCategorySelected && (
                      <Button
                        variant='outlined'
                        className={classes.bulkAction}
                        onClick={handleEditClick}
                      >
                        Edit
                      </Button>
                    )}
                  </div>
                </div>
              )}
              <DataGrid
                autoHeight
                className={classes.dataGrid}
                rows={categories}
                columns={columns}
                pageSize={10}
                checkboxSelection
                onSelectionModelChange={handleOnSelectionChange}
                rowsPerPageOptions={[5, 10, 20, 50, 100]}
                selectionModel={selectedCategory}
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
      </Card>
    </Fragment>
  );
};

CategoryListView.propTypes = {
  categories: PropTypes.array,
  editData: PropTypes.func.isRequired,
};

export default CategoryListView;
