import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  FormHelperText,
  Grid,
  TextField,
} from "@material-ui/core";
import { useSnackbar } from "notistack";
import { Formik } from "formik";
import * as Yup from "yup";
import axios from "axios";

const Security = () => {
  const { enqueueSnackbar } = useSnackbar();

  return (
    <Formik
      initialValues={{
        oldPassword: "",
        password: "",
        passwordConfirm: "",
      }}
      validationSchema={Yup.object().shape({
        oldPassword: Yup.string().max(255).required("Required"),
        password: Yup.string()
          .min(7, "Must be at least 7 characters")
          .max(255)
          .required("Required"),
        passwordConfirm: Yup.string()
          .oneOf([Yup.ref("password"), null], "Passwords must match")
          .required("Required"),
      })}
      onSubmit={async (values, props) => {
        const { resetForm, setErrors, setStatus, setSubmitting } = props;
        try {
          const config = {
            headers: {
              "Content-Type": "application/json",
            },
          };

          const body = {
            oldPassword: values.oldPassword,
            newPassword: values.password,
          };

          await axios
            .patch("/api/users/changePassword", body, config)
            .then((resp) => {
              resetForm();
              setSubmitting(false);
              setStatus({ success: true });
              enqueueSnackbar("Password updated", { variant: "success" });
            })
            .catch((error) => {
              setErrors({
                submit: error.response.data.message,
              });
              enqueueSnackbar(
                `Password failed to update: ${error.response.data.message}`,
                { variant: "error" }
              );
            });
        } catch (error) {
          setSubmitting(true);
          setErrors({
            submit: error.message,
          });
          setStatus({ success: false });
          enqueueSnackbar("Password update failed", { variant: "error" });
        }
      }}
    >
      {(props) => {
        const {
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          touched,
          values,
        } = props;
        return (
          <form onSubmit={handleSubmit}>
            <Card>
              <CardHeader title='Change password' />
              <Divider />
              <CardContent>
                <Grid
                  container
                  spacing={3}
                  justify='center'
                  alignItems='center'
                >
                  <Grid item md={4} sm={6} xs={12}>
                    <TextField
                      error={Boolean(touched.oldPassword && errors.oldPassword)}
                      fullWidth
                      helperText={touched.oldPassword && errors.oldPassword}
                      label='Current Password'
                      name='oldPassword'
                      type='password'
                      onBlur={handleBlur}
                      value={values.oldPassword}
                      variant='outlined'
                      onChange={handleChange}
                      disabled={isSubmitting}
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  justify='center'
                  alignItems='center'
                >
                  <Grid item md={4} sm={6} xs={12}>
                    <TextField
                      error={Boolean(touched.password && errors.password)}
                      fullWidth
                      helperText={touched.password && errors.password}
                      label='New Password'
                      name='password'
                      type='password'
                      onBlur={handleBlur}
                      value={values.password}
                      variant='outlined'
                      onChange={handleChange}
                      disabled={isSubmitting}
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  justify='center'
                  alignItems='center'
                >
                  <Grid item md={4} sm={6} xs={12}>
                    <TextField
                      error={Boolean(
                        touched.passwordConfirm && errors.passwordConfirm
                      )}
                      fullWidth
                      helperText={
                        touched.passwordConfirm && errors.passwordConfirm
                      }
                      label='Confirm Password'
                      name='passwordConfirm'
                      type='password'
                      onBlur={handleBlur}
                      value={values.passwordConfirm}
                      variant='outlined'
                      onChange={handleChange}
                      disabled={isSubmitting}
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  justify='center'
                  alignItems='center'
                >
                  <Box
                    mt={3}
                    textAlign='center'
                    alignSelf='center'
                    alignContent='center'
                    alignItems='center'
                  >
                    {errors.submit && (
                      <Box
                        mt={2}
                        mb={2}
                        textAlign='center'
                        alignSelf='center'
                        alignContent='center'
                        alignItems='center'
                      >
                        <FormHelperText error>
                          {errors.submit}
                        </FormHelperText>
                      </Box>
                    )}
                  </Box>
                </Grid>
              </CardContent>
              <Divider />
              <Box p={2} display='flex' justifyContent='flex-end'>
                <Button
                  color='primary'
                  disabled={isSubmitting}
                  variant='contained'
                  type='submit'
                >
                  Update Password
                </Button>
              </Box>
            </Card>
          </form>
        );
      }}
    </Formik>
  );
};

export default Security;
