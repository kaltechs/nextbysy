import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {
    Box,
    Button,
  Card,
  CardContent,
  CardHeader,
  Checkbox,
  Divider,
  FormControlLabel,
  Grid,
  makeStyles,
  Tooltip,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles(() => ({
  root: {},
}));

const Notifications = ({ className, ...rest }) => {
  const classes = useStyles();

  const handleSubmit = async (event) => {
    event.preventDefault();
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card className={classNames(classes.root, className)} {...rest}>
        <CardHeader title='Notifications' />
        <Divider />
        <CardContent>
          <Grid container spacing={6} wrap='wrap'>
            <Grid item md={4} sm={6} xs={12}>
              <Typography gutterBottom variant='h6' color='textPrimary'>
                System
              </Typography>
              <Typography gutterBottom variant='body2' color='textSecondary'>
                You will recieve emails in your business email address
              </Typography>
              <div>
                <FormControlLabel
                  control={<Checkbox defaultChecked color='primary' />}
                  label='Email alerts'
                />
              </div>
              <div>
                <Tooltip title='Currently not available'>
                  <FormControlLabel
                    control={<Checkbox disabled color='primary' />}
                    label='Push Notifications'
                  />
                </Tooltip>
              </div>
              <div>
                <FormControlLabel
                  control={<Checkbox color='primary' />}
                  label='Phone calls'
                />
              </div>
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <Box p={2} display='flex' justifyContent='flex-end'>
            <Button color='primary' type='submit' variant='contained'>Save Settings</Button>
        </Box>
      </Card>
    </form>
  );
};

export default Notifications;
