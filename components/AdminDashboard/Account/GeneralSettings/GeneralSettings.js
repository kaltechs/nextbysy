import { Grid } from "@material-ui/core";
import ProfileData from "./ProfileData";
import ProfileImage from "./ProfileImage";
import { useUser } from "../../../../hooks/useUser";
import dynamic from "next/dynamic";


const General = ({ className, ...rest }) => {
  const { user, isLoading } = useUser();

  if (isLoading) {
    const SplashScreen = dynamic(() => import ('../../../SplashScreen'));
    return <SplashScreen />
  }

  return (
    <Grid
      className={className}
      container
      spacing={3}
      {...rest}
    >
      <Grid item lg={4} md={6} xl={3} xs={12}>
        <ProfileImage user={user} />
      </Grid>
      <Grid item lg={8} md={6} xl={9} xs={12}>
        <ProfileData user={user} />
      </Grid>
    </Grid>
  );
};

export default General;