import PropTypes from "prop-types";
import {
  Card,
  Divider,
  CardHeader,
  makeStyles,
  CardContent,
  TextField,
  Grid,
  Box,
  FormHelperText,
  Button,
} from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import classNames from "classnames";
import LoadingScreen from "../../../LoadingScreen";
import { useSnackbar } from "notistack";
import { Fragment, useState } from "react";
import axios from "axios";

const useStyles = makeStyles(() => ({
  root: {},
}));

const ProfileData = ({ user, className, ...rest }) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const [pending, setPending] = useState(false);
  const [data, setData] = useState({
    email: user.email || "",
    firstName: user.firstName || "",
    lastName: user.lastName || "",
    phone: user.phone || "",
    country: user.country || "",
    city: user.city || "",
    state: user.state || "",
  });

  return (
    <Fragment>
      {pending ? (
        <LoadingScreen />
      ) : (
        <Formik
          enableReinitialize
          initialValues={data}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("Must be a valid email")
              .max(255)
              .required("Email is required"),
            firstName: Yup.string().required("Field is required"),
            lastName: Yup.string().required("Field is required"),
            phone: Yup.string(),
            country: Yup.string().max(255),
            city: Yup.string().max(255),
            state: Yup.string().max(255),
          })}
          onSubmit={async (
            values,
            { resetForm, setErrors, setStatus, setSubmitting }
          ) => {
            try {
              setSubmitting(true);
              const config = {
                headers: {
                  "Content-Type": "application/json",
                },
              };
              await axios
                .patch(`/api/users/${user._id}`, values, config)
                .then((resp) => {
                  setData(values);
                  //   setUserData({
                  //     user: { ...values },
                  //   });
                  setSubmitting(false);
                  setStatus({ success: true });
                  setPending(false);
                  resetForm();
                  enqueueSnackbar("Profile updated", {
                    variant: "success",
                  });
                });
            } catch (err) {
              setStatus({ success: false });
              setErrors({ submit: err.response.data.message });
              setSubmitting(false);
              setPending(false);
              enqueueSnackbar(err.response.data.message, {
                variant: "error",
              });
            }
          }}
        >
          {({
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            touched,
            values,
          }) => (
            <form onSubmit={handleSubmit}>
              <Card className={classNames(classes.root, className)} {...rest}>
                <CardHeader title='Profile' />
                <Divider />
                <CardContent>
                  <Grid container spacing={4}>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.firstName && errors.firstName)}
                        helperText={touched.firstName && errors.firstName}
                        name='firstName'
                        label='First Name'
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.firstName}
                        variant='outlined'
                        fullWidth
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.lastName && errors.lastName)}
                        helperText={touched.lastName && errors.lastName}
                        name='lastName'
                        label='Last Name'
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.lastName}
                        variant='outlined'
                        fullWidth
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.email && errors.email)}
                        helperText={touched.email && errors.email}
                        name='email'
                        label='Email'
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.email}
                        variant='outlined'
                        fullWidth
                        disabled
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.phone && errors.phone)}
                        helperText={touched.phone && errors.phone}
                        label='Phone No.'
                        name='phone'
                        value={values.phone}
                        fullWidth
                        variant='outlined'
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.country && errors.country)}
                        helperText={touched.country && errors.country}
                        fullWidth
                        variant='outlined'
                        value={values.country}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        label='Country'
                        name='country'
                        value={values.country}
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.state && errors.state)}
                        helperText={touched.state && errors.state}
                        fullWidth
                        variant='outlined'
                        value={values.state}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        label='State'
                        name='state'
                        value={values.state}
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.city && errors.city)}
                        helperText={touched.city && errors.city}
                        fullWidth
                        variant='outlined'
                        value={values.city}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        label='City'
                        name='city'
                        value={values.city}
                      />
                    </Grid>
                  </Grid>
                  {errors.submit && (
                  <Box mt={3}>
                    <FormHelperText error>error error error</FormHelperText>
                  </Box>
                  )}
                </CardContent>
                <Divider />
                <Box p={2} display='flex' justifyContent='flex-end'>
                  <Button
                    color='primary'
                    disabled={isSubmitting}
                    variant='contained'
                    type='submit'
                  >
                    Save Changes
                  </Button>
                </Box>
              </Card>
            </form>
          )}
        </Formik>
      )}
    </Fragment>
  );
};

ProfileData.propTypes = {
  user: PropTypes.object,
};

export default ProfileData;
