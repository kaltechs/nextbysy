import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { useSnackbar } from "notistack";
import Avatar from "../../../Avatar";
import { useRef, useState } from "react";
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  root: {},
  name: {
    marginTop: theme.spacing(1),
  },
  avatar: {
    height: 100,
    width: 100,
  },
  fileInput: {
    display: "none",
  },
}));

const ProfileImage = ({ user }) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const [pending, setPending] = useState(false);
  const [avatar, setAvatar] = useState(user.avatar);
  const fileInputRef = useRef(null);

  const handleAttach = () => {
    fileInputRef.current.click();
  };

  const handleImageUpload = async (e) => {
    e.preventDefault();

    const uploadedFile = e.target.files[0];
    const formData = new FormData();
    formData.append("avatar", uploadedFile);
    setPending(true);
    const config = {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    await axios
      .post("/api/users/avatar", formData, config)
      .then((resp) => {
        setAvatar(resp.data);
        setPending(false);
        enqueueSnackbar("Profile updated", {
          variant: "success",
        });
      })
      .catch((err) => {
        enqueueSnackbar(err.response.data.message, {
          variant: "error",
        });
        setPending(false);
      });
  };

  return (
    <Card className={classes.root}>
      <CardContent>
        <Box
          display='flex'
          alignItems='center'
          flexDirection='column'
          textAlign='center'
        >
          <Avatar
            className={classes.avatar}
            avatar={avatar}
            onClick={handleAttach}
            style={{ cursor: "pointer" }}
          />
          <Box mt={1}>
            <Typography
              className={classes.name}
              color='textPrimary'
              gutterBottom
              variant='h5'
              mt={1}
            >
              {user.firstName} {user.lastName}
            </Typography>
          </Box>
        </Box>
      </CardContent>
      <CardActions>
        <Button
          fullWidth
          color='primary'
          variant='contained'
          onClick={handleAttach}
          disabled={pending}
        >
          Update Image
        </Button>
        <input
          className={classes.fileInput}
          ref={fileInputRef}
          onChange={handleImageUpload}
          type='file'
        />
      </CardActions>
    </Card>
  );
};

export default ProfileImage;
