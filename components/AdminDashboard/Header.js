import { Typography } from "@material-ui/core";
import Link from "next/link";
import PropTypes from "prop-types";

const Header = ({ className, current, ...rest }) => {

  return (
    <div className={className} {...rest}>
      <Link href='/admin/dashboard'>
        <Typography color='inherit' style={{cursor: 'pointer'}}>Dashboard</Typography>
      </Link>
      <Typography variant='h3' color='primary'>
        {current}
      </Typography>
    </div>
  );
};

Header.propTypes = {
  current: PropTypes.string,
};

export default Header;
