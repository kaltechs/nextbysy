const mongoose = require("mongoose");

const ExpoSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      require: true,
    },
    companyName: {
      type: String,
    },
    email: {
      type: String,
      require: true,
      unique: true,
    },
    category: {
      type: String,
    },
    cardContent: {
      type: String,
    },
    intro: {
      type: String,
    },
    curiosity: {
      type: String,
    },
    isDraft: {
      type: Boolean,
    },
    isPublished: {
      type: Boolean,
    },
    expoimg: {
      type: String,
    },
    introVideoUrl: {
      type: String,
    },
    spokenLanguages: [
      {
        name: {
          type: String,
        },
        value: {
          type: Number,
        },
      },
    ],
    headquarters: [
      {
        name: {
          type: String,
        },
        value: {
          type: String,
        },
      },
    ],
    prices: [
      {
        name: {
          type: String,
        },
        value: {
          type: String,
        },
      },
    ],
  },
  {
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
  }
);

const Expo = mongoose.models.expo || mongoose.model("expo", ExpoSchema);

export default Expo;
