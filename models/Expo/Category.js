const mongoose = require("mongoose");

const CategorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      require: true,
      unique: true,
    },
    slug: {
      type: String,
      require: true,
      unique: true,
    },
    color: {
      type: String,
      unique: true
    }
  },
  {
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
  }
);

const ExpoCategory =
  mongoose.models.expoCategory ||
  mongoose.model("expoCategory", CategorySchema);

export default ExpoCategory;
