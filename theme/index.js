import _ from "lodash";
import {
  createMuiTheme,
  responsiveFontSizes,
} from "@material-ui/core";
import { THEMES } from "../util/enums";
import { softShadows, strongShadows } from "./shadows";
import typography from "./typography";
import useDarkMode from "use-dark-mode";

const baseOptions = {
  direction: "ltr",
  typography,
  overrides: {
    MuiInputBase: {
      input: {
        padding: "13.5px 14px",
      },
    },
    MuiLinearProgress: {
      root: {
        borderRadius: 3,
        overflow: "hidden",
      },
    },
    MuiListItemIcon: {
      root: {
        minWidth: 32,
      },
    },
    MuiChip: {
      root: {
        backgroundColor: "rgba(0,0,0,0.075)",
      },
    },
  },
};

const themesOptions = [
  {
    name: THEMES.LIGHT,
    overrides: {
      MuiInputBase: {
        input: {
          "&::placeholder": {
            opacity: 1,
            color: '#546e7a',
          },
        },
      },
      MuiTableRow: {
        root: {
          "&$selected": {
            backgroundColor: "rgba(209, 142, 94, 0.1)",
            "&:hover": {
              backgroundColor: "rgba(209, 142, 94, 0.5)",
            },
          },
        },
      },
    },
    palette: {
      type: "light",
      action: {
        active: '#546e7a',
      },
      background: {
        default: '#fff',
        dark: "#f4f6f8",
        paper: '#fff',
      },
      primary: {
        main: "#D18E5E",
        light: "#FFFFFF",
      },
      secondary: {
        main: "#5850EC",
      },
      text: {
        primary: '#263238',
        secondary: '#546e7a',
      },
    },
    shadows: softShadows,
  },
  {
    name: THEMES.ONE_DARK,
    overrides: {
      MuiInputBase: {
        input: {
          "&::placeholder": {
            opacity: 1,
            color: '#adb0bb',
          },
          "&$focused": {
            backgroundColor: "rgba(0, 0, 0, 0.18)",
          },
          "&:-webkit-autofill": {
            "-webkit-box-shadow": "0 0 0 100px #282C34 inset !important",
          },
        },
      },
      MuiOutlinedInput: {
        input: {
          fontSize: "14px",
        },
      },
      MuiTableRow: {
        root: {
          "&$selected": {
            backgroundColor: "rgba(209, 142, 94, 0.1)",
          },
        },
      },
    },
    palette: {
      type: "dark",
      action: {
        active: "rgba(255, 255, 255, 0.54)",
        hover: "rgba(255, 255, 255, 0.04)",
        selected: "rgba(255, 255, 255, 0.08)",
        disabled: "rgba(255, 255, 255, 0.26)",
        disabledBackground: "rgba(255, 255, 255, 0.12)",
        focus: "rgba(255, 255, 255, 0.12)",
      },
      background: {
        default: "#282C34",
        dark: "#1c2025",
        paper: "#282C34",
      },
      primary: {
        main: "#D18E5E",
        contrastText: "#FFFFFF",
        error: 'red'
      },
      secondary: {
        main: "#282C34",
      },
      text: {
        primary: "#e6e5e8",
        secondary: "#adb0bb",
      },
    },
    shadows: strongShadows,
  },
];

export const createTheme = () => {
  const { value: isDark } = useDarkMode(true);
  const selectedTheme = isDark ? THEMES.ONE_DARK : THEMES.LIGHT
  let themeOptions = themesOptions.find((theme) => theme.name === selectedTheme);

  let theme = createMuiTheme(
    _.merge({}, baseOptions, themeOptions, { direction: 'ltr' })
  );

  theme = responsiveFontSizes(theme);

  return theme;
};
