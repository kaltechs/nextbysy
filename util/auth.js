import bcrypt from 'bcryptjs';

export const hashPassword = async(password) => {
    const salt  = await bcrypt.genSalt(12);
    const hashedPassword = await bcrypt.hash(password, salt);

    return hashPassword;
}

export const comparePassword = async (oldPassword, newPassword) => {
    const result = await bcrypt.compare(newPassword, oldPassword);

    return result;
}