export const THEMES = {
  LIGHT: "LIGHT",
  ONE_DARK: "ONE_DARK",
};

export const LOCATION_TYPE = [
  {
    name: "Hotel",
  },
  {
    name: "Motel",
  },
  {
    name: "Pension",
  },
];

export const STATUS_CODE = {
  BadRequest: 400,
  Unauthorized: 401,
  Forbidden: 403,
  NotFound: 404,
  MethodNotAllowed: 405,
};

export const LANGUAGES = [
  {
    name: "English",
  },
  {
    name: "Hungarian",
  },
  {
    name: "Romanian",
  },
  {
    name: "Spanish",
  },
];

export const PUBLIC_MENUS = [
  {
    title: "Főoldal",
    link: '/'
  },
  {
    title: "Helyszínek",
    link: '/locations'
  },
  {
    title: "Magazin",
    link: '/magazin'
  },
  {
    title: "Kiállítás",
    link: '/expo'
  },
  {
    title: "Galéria",
    link: '/gallery'
  },
  {
    title: "Kapcsolat",
    link: '/contact'
  },
  {
    title: 'Belépés',
    link: '/publicLogin',
    hideToLoggedUser: true
  },
  {
    title: 'Kilépés',
    link: '/publicLogout'
  }
];
