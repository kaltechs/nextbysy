import axios from "axios";
import useSWR from "swr";

export const fetcher = (url) => axios.get(url).then((res) => res.data);

export const useRequest = (request, { initialData, ...config } = {}) => {
  return useSWR(
    request && JSON.stringify(request),
    () => axios(request || {}).then((response) => response.data),
    {
      ...config,
      initialData: initialData && {
        status: 200,
        statusText: "InitialData",
        headers: {},
        data: initialData,
      },
    }
  );
};
