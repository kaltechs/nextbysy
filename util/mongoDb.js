import mongoose from "mongoose";
import bcrypt from "bcryptjs";

const connection = {};

export const connectToDatabase = async () => {
  if (connection.isConnected) {
    return;
  }

  const db = await mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  });

  connection.isConnected = db.connections[0].readyState;
  console.log("Db Connected");
};

export const isValidObjectId = (objectId) => {
  return mongoose.isValidObjectId(objectId);
};

export const comparePassword = async (newPassword, oldPassword) => {
  const result = await bcrypt.compare(oldPassword, newPassword);
  return result;
};

export const hashPassword = async (password) => {
  const salt = await bcrypt.genSalt(12);
  const hashedPassword = await bcrypt.hash(password, salt);
  return hashedPassword;
};
