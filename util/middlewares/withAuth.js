import { getSession } from "next-auth/client"

const auth = () => {
    return async (req, res, next) => {
        const session = await getSession({req:req});

        req.user = {
            isAuthenticated: false,
            isAdmin: false,
            userEmail: null,
          };
        
          if (!session) {
            next();
          } else {
            req.user.isAuthenticated = true;
            req.user.isAdmin = session.user.isAdmin;
            req.user.userEmail = session.user.email;
        
            next();
          }
    }
}

export default auth; 