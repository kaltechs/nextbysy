import { connectToDatabase } from "../mongoDb";

const db = () => {
  return async (req, res, next) => {
    try {
      await connectToDatabase();
      next();
    } catch (error) {
      throw new Error(`Can't connect to db ${error.message}`);
    }
  };
};

export default db;
