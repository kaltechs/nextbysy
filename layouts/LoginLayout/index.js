import GlobalStyles from "../layoutGlobalStyles";

const LoginLayout = ({ children }) => {
  return (
    <>
      <GlobalStyles />
      {children}
    </>
  );
};

export default LoginLayout;