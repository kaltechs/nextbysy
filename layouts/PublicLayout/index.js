import Head from 'next/head';
import { GoogleFonts } from "next-google-fonts"
import Header from '../../components/Header';

const PublicLayout = (pageProps) => {
    return (
        <div>
            <GoogleFonts href="https://fonts.googleapis.com/css2?family=Playfair+Display:wght@700&family=Poppins:ital,wght@0,300;0,400;0,700;1,400&display=swap" />
            <Head>
                <title>{pageProps.title} - Before You Say Yes</title>
                <link rel="icon" href="/favicon.png" />
            </Head>
            <Header />
            <main>{pageProps.children}</main>
        </div>
    )
}

export default PublicLayout;