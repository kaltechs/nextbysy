import Link from "next/link";
import {
  AppBar,
  Box,
  Hidden,
  IconButton,
  makeStyles,
  SvgIcon,
  Toolbar,
  Tooltip,
} from "@material-ui/core";
import { Menu as MenuIcon } from "react-feather";
import Logo from "../../../components/Logo";
import {
  Brightness7 as LightIcon,
  Brightness4 as DarkIcon,
} from "@material-ui/icons";
import { THEMES } from "../../../util/enums";
import Account from "./Account";
import useDarkMode from "use-dark-mode";

const useStyles = makeStyles((theme) => ({
  root: {
    zIndex: theme.zIndex.drawer + 100,
    ...(theme.name === THEMES.LIGHT
      ? {
          backgroundColor: theme.palette.primary.light,
        }
      : {}),
    ...(theme.name === THEMES.ONE_DARK
      ? {
          backgroundColor: theme.palette.background.default,
        }
      : {}),
  },
  toolbar: {
    minHeight: 64,
  },
}));

const TopBar = ({ onMobileNavOpen, ...rest }) => {
  const classes = useStyles();
  const darkMode = useDarkMode(false);

  const handleThemeChange = () => {
    darkMode.toggle();
  };

  return (
    <AppBar className={classes.root}>
      <Toolbar className={classes.toolbar}>
        <Hidden lgUp>
          <IconButton color='inherit' onClick={onMobileNavOpen}>
            <SvgIcon fontSize='small'>
              <MenuIcon />
            </SvgIcon>
          </IconButton>
        </Hidden>
        <Hidden mdDown>
          <Link href='/'>
            <a>
              <Logo width='50px' style={{ paddingTop: "10px" }} />
            </a>
          </Link>
        </Hidden>
        <Box ml={2} flexGrow={1} />
        <Box ml={2}>
          {darkMode.value ? (
            <Tooltip title='Light'>
              <IconButton onClick={handleThemeChange}>
                <LightIcon color='primary' />
              </IconButton>
            </Tooltip>
          ) : (
            <Tooltip title='Dark'>
              <IconButton onClick={handleThemeChange}>
                <DarkIcon color='primary' />
              </IconButton>
            </Tooltip>
          )}
        </Box>
        <Box ml={2}>
          <Account />
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export default TopBar;
