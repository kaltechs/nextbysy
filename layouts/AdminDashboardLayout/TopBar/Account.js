import {
  Box,
  makeStyles,
  Menu,
  MenuItem,
  IconButton,
  SvgIcon,
  Tooltip,
} from "@material-ui/core";
import { signOut } from "next-auth/client";
import { useRouter } from "next/router";
import { Fragment, useRef, useState } from "react";
import { Settings as SettingsIcon } from "react-feather";

const useStyles = makeStyles((theme) => ({
  avatar: {
    height: 32,
    width: 32,
    marginRight: theme.spacing(1),
  },
  popover: {
    width: 200,
  },
}));

const Account = () => {
  const classes = useStyles();
  const ref = useRef(null);
  const [isOpen, setOpen] = useState(false);
  const router = useRouter();

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleRedirect = (url) => {
    router.push(url);
  };

  const handleLogout = (e) => {
    e.preventDefault();
    signOut({
      callbackUrl: `${process.env.NEXT_PUBLIC_BASE_URL}/auth/login`,
    });
  };

  return (
    <Fragment>
      <Tooltip title='Settings'>
        <Box
          display='flex'
          alignItems='center'
          component={IconButton}
          onClick={handleOpen}
          ref={ref}
        >
          <SvgIcon fontSize='small' color='primary'>
            <SettingsIcon />
          </SvgIcon>
        </Box>
      </Tooltip>
      <Menu
        onClose={handleClose}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        keepMounted
        PaperProps={{ className: classes.popover }}
        getContentAnchorEl={null}
        open={isOpen}
        anchorEl={ref.current}
      >
        <MenuItem onClick={() => handleRedirect("/app/social/profile")}>
          Profile
        </MenuItem>
        <MenuItem onClick={() => handleRedirect("/app/account")}>
          Account
        </MenuItem>
        <MenuItem onClick={handleLogout}>Logout</MenuItem>
      </Menu>
    </Fragment>
  );
};

export default Account;
