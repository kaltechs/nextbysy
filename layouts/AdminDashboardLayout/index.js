import { makeStyles } from "@material-ui/core";
import { useState } from "react";
import Navbar from "./Navbar";
import TopBar from "./TopBar";
import Proptypes from "prop-types";
import CssBaseline from "@material-ui/core/CssBaseline";
import { ProtectRoute } from "../../components/ProtectRoute";
import GlobalStyles from "../layoutGlobalStyles";
import PerfectScrollbar from "react-perfect-scrollbar";

import { SnackbarProvider } from "notistack";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    display: "flex",
    height: "100%",
    overflow: "hidden",
    width: "100%",
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
  },
  wrapper: {
    display: "flex",
    flex: "1 1 auto",
    overflow: "hidden",
    paddingTop: 64,
    [theme.breakpoints.up("lg")]: {
      paddingLeft: 256,
    },
    backgroundColor: theme.palette.background.dark,
  },
  contentContainer: {
    display: "flex",
    flex: "1 1 auto",
    overflow: "hidden",
    backgroundColor: theme.palette.background.dark + "!important",
  },
  content: {
    flex: "1 1 auto",
    height: "100%",
    overflow: "auto",
    backgroundColor: theme.palette.background.dark + "!important",
  },
}));

const DashboardLayout = ({ children }) => {
  const classes = useStyles();
  const [isMobileNavOpen, setMobileNavOpen] = useState(false);

  return (
    <ProtectRoute>
      <GlobalStyles />
      <SnackbarProvider dense maxSnack={3}>
        <CssBaseline />
        <div className={classes.root}>
          <TopBar onMobileNavOpen={() => setMobileNavOpen(true)} />
          <Navbar
            openMobile={isMobileNavOpen}
            onMobileClose={() => setMobileNavOpen(false)}
          />
          <div className={classes.wrapper}>
            <div className={classes.contentContainer}>
              <div className={classes.content}>
                <PerfectScrollbar>{children}</PerfectScrollbar>
              </div>
            </div>
          </div>
        </div>
      </SnackbarProvider>
    </ProtectRoute>
  );
};

DashboardLayout.propTypes = {
  children: Proptypes.node,
};

export default DashboardLayout;
