import {
  Box,
  Divider,
  Drawer,
  Hidden,
  List,
  ListSubheader,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import {
  PieChart as PieChartIcon,
  User as UserIcon,
  Users as UsersIcon,
  Aperture as ApertureIcon,
  Home as HomeIcon,
} from "react-feather";
import { useRouter } from "next/router";
import { useSession } from "next-auth/client";
import PerfectScrollbar from "react-perfect-scrollbar";
import NavItem from "./NavItem";
import Link from "next/link";
import Avatar from "../../../components/Avatar";

const useStyles = makeStyles(() => ({
  mobileDrawer: {
    width: 256,
  },
  desktopDrawer: {
    width: 256,
    top: 64,
    height: "calc(100% - 64px)",
  },
  avatar: {
    cursor: "pointer",
    width: 64,
    height: 64,
  },
}));

const sections = [
  {
    subheader: "Reports",
    items: [
      {
        title: "Dashboard",
        icon: PieChartIcon,
        href: "/admin/dashboard",
      },
    ],
  },
  {
    subheader: "Management",
    items: [
      {
        title: "Users",
        icon: UsersIcon,
        href: "/admin/users",
        items: [
          {
            title: "Users List",
            href: "/admin/users",
          },
          {
            title: "View Customer",
            href: "/app/management/customers/1",
          },
          {
            title: "Edit Customer",
            href: "/app/management/customers/1/edit",
          },
        ],
      },
      {
        title: "Expo",
        icon: ApertureIcon,
        href: "/admin/expo",
        items: [
          {
            title: "Category",
            href: "/admin/expo/categories",
          },
          {
            title: "Expo List",
            href: "/admin/expo",
          },
          {
            title: "Add Expo Item",
            href: "/admin/expo/addexpo",
          },
        ],
      },
      {
        title: "Locations",
        icon: HomeIcon,
        href: "/app/management/location",
        items: [
          {
            title: "Add New",
            href: "/app/management/AddLocation",
          },
        ],
      },
    ],
  },
  {
    subheader: "Settings",
    items: [
      {
        title: "Account",
        href: "/admin/account",
        icon: UserIcon,
      },
    ],
  },
];

function renderNavItems({ items, pathname, depth = 0 }) {
  return (
    <List disablePadding>
      {items.reduce(
        (acc, item) => reduceChildRoutes({ acc, item, pathname, depth }),
        []
      )}
    </List>
  );
}

function reduceChildRoutes({ acc, pathname, item, depth }) {
  const key = item.title + depth;

  if (item.items) {
    const open = pathname.startsWith(item.href);

    acc.push(
      <NavItem
        depth={depth}
        icon={item.icon}
        info={item.info}
        key={key}
        open={Boolean(open)}
        title={item.title}
      >
        {renderNavItems({
          depth: depth + 1,
          pathname,
          items: item.items,
        })}
      </NavItem>
    );
  } else {
    acc.push(
      <NavItem
        depth={depth}
        href={item.href}
        icon={item.icon}
        info={item.info}
        key={key}
        title={item.title}
      />
    );
  }

  return acc;
}

const Navbar = ({ onMobileClose, openMobile }) => {
  const router = useRouter();
  const [session, loading] = useSession();
  const classes = useStyles();

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
  }, [router.pathname]);

  const content = (
    <Box height='100%' display='flex' flexDirection='column'>
      <PerfectScrollbar options={{ suppressScrollX: true }}>
        <Hidden lgUp>
          <Box p={2} display='flex' justifyContent='center'>
            <Link href='/'>
              <p>BeforeYouSayYes</p>
            </Link>
          </Box>
        </Hidden>
        <Box p={3}>
          <Box display='flex' justifyContent='center'>
            <Link href='/app/account'>
              <a>
                <Avatar
                  avatar={session.user.avatar}
                  alt='user'
                  className={classes.avatar}
                />
              </a>
            </Link>
          </Box>
          <Box display='flex' justifyContent='center' mt={2}>
            <Link href='/app/account'>
              <Typography variant='h5' color='textPrimary'>
                <a>
                  {session.user.firstName} {session.user.lastName}
                </a>
              </Typography>
            </Link>
          </Box>
        </Box>
        <Divider />
        <Box p={2}>
          {sections.map((section) => {
            return (
              <List
                key={section.subheader}
                subheader={
                  <ListSubheader disableGutters disableSticky>
                    {section.subheader}
                  </ListSubheader>
                }
              >
                {renderNavItems({
                  items: section.items,
                  pathname: router.pathname,
                })}
              </List>
            );
          })}
        </Box>
        <Divider />
      </PerfectScrollbar>
    </Box>
  );

  return (
    <Fragment>
      <Hidden lgUp>
        <Drawer
          anchor='left'
          classes={{ paper: classes.mobileDrawer }}
          onClose={onMobileClose}
          open={openMobile}
          variant='temporary'
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden mdDown>
        <Drawer
          anchor='left'
          classes={{ paper: classes.desktopDrawer }}
          open
          variant='persistent'
        >
          {content}
        </Drawer>
      </Hidden>
    </Fragment>
  );
};

Navbar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool,
};

export default Navbar;
