import Link from "next/link"
import dynamic from 'next/dynamic'

const PublicLayout = dynamic(() => import('../layouts/PublicLayout'));

const LandingPage = () => {
  return (
    <div>
      Landing Page
      <Link href='/admin/dashboard'>Dashboard</Link>
      <Link href='/expo'>Expo page</Link>
    </div>
  );
};

LandingPage.layout = PublicLayout;
LandingPage.title = 'Home';

export default LandingPage;
  