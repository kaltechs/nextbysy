import dynamic from "next/dynamic";
import Link from "next/link";
import generalStyles from "../scss/Modules/SectionHeader.module.scss";
import classes from "../scss/Expo/expoLanding.module.scss";
import {
  Col,
  Row,
  Container,
  CustomInput,
  Card,
  CardBody,
  CardText,
} from "reactstrap";
import clsx from "classnames";
import axios from "axios";
import { useEffect, useState } from "react";
import useSWR from "swr";
import { fetcher } from "../util/fetcher";

const PublicLayout = dynamic(() => import("../layouts/PublicLayout"));

const ExpoLandingPage = ({ expoCategories, expoItems }) => {
  const [expo, setExpo] = useState(expoItems);
  const [filteredItems, setFilteredItems] = useState(expoItems);
  const [selectedFilter, setSelectedFilter] = useState([]);

  console.log("filteredItems", filteredItems);

  const { data: categories, error: catError } = useSWR(
    "/api/expo/category",
    fetcher,
    {
      initialData: expoCategories,
    }
  );

  const { data: expoList, error: expoError } = useSWR("/api/expo", fetcher, {
    initialData: expoItems,
  });

  useEffect(() => {
    if (expoList) {
      setExpo(expoList);
    }
  }, [expoList]);

  useEffect(() => {
    applyFilter();
  }, [selectedFilter]);

  const applyFilter = () => {
    let newFiletredExpoItems = [];

    selectedFilter.forEach((item) => {
      const filItem = expo.filter((value) => value.category === item)[0];
      if (filItem) {
        newFiletredExpoItems.push(filItem);
      }
    });

    setFilteredItems(newFiletredExpoItems);
  };

  const onFilterChange = (e) => {
    if (!selectedFilter.includes(e.target.name)) {
      setSelectedFilter([...selectedFilter, e.target.name]);
    } else {
      const newSelectedFilterList = selectedFilter.filter(
        (cat) => cat !== e.target.name
      );
      setSelectedFilter(newSelectedFilterList);
    }
  };

  return (
    <section className='py-5'>
      <Container fluid>
        <div className={generalStyles.sectionHeader}>
          <Row className='align-items-end'>
            <Col lg={4} className={generalStyles.sectionTitleContainer}>
              <h1>Expo</h1>
            </Col>
            <Col lg={8} className={generalStyles.sectionDescriptionContainer}>
              <p>
                A Le Til Kúriához foghatót nehezen találni országunkban: a
                káprázatos hófehér kastély, az épületet körülölelő mesés park és
                lélegzetelállító kültéri kápolna azonnal elrabolta a szívünket.{" "}
              </p>
            </Col>
          </Row>
        </div>
        <div className={classes.expoContainer}>
          <Col lg={3}>
            <h2 className={classes.filterTitle}>Szűrés</h2>
            <ul className='list-unstyled mb-0'>
              {categories.map((item, i) => {
                return (
                  <li key={i}>
                    <CustomInput
                      type='checkbox'
                      id={item.slug}
                      name={item.slug}
                      label={item.name}
                      onChange={onFilterChange}
                    />
                  </li>
                );
              })}
            </ul>
          </Col>
          <Col lg={9}>
            {filteredItems?.map((filtItem, i) => {
              return (
                <Card key={i}>
                  <CardBody>
                    <CardBody>
                      <CardText>{filtItem.email}</CardText>
                    </CardBody>
                  </CardBody>
                </Card>
              );
            })}
          </Col>
        </div>
      </Container>
    </section>
  );
};

ExpoLandingPage.layout = PublicLayout;
ExpoLandingPage.title = "Expo";

export default ExpoLandingPage;

export const getStaticProps = async () => {
  let data = [];
  let expoItems = [];

  await axios
    .get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/expo/category`)
    .then((resp) => {
      data = resp.data;
    })
    .catch((error) => {
      console.log(error);
    });

  await axios
    .get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/expo`)
    .then((resp) => {
      expoItems = resp.data;
    })
    .catch((error) => {
      console.log(error);
    });

  return {
    props: {
      expoCategories: data,
      expoItems,
    },
    revalidate: 10,
  };
};
