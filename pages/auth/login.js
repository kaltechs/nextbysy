import { makeStyles } from "@material-ui/core";

import FormHelperText from "@material-ui/core/FormHelperText";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";

import { Formik } from "formik";
import { Fragment } from "react";
import * as Yup from "yup";
import classNames from "classnames";
import { signIn } from "next-auth/client";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
import Logo from '../../components/Logo';
import { useUser } from '../../hooks/useUser';
import { trigger } from "swr";

const LoginLayout = dynamic(() => import("../../layouts/LoginLayout"));

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
  },
  container: {
    paddingRight: "15px",
    paddingLeft: "15px",
    marginRight: "auto",
    marginLeft: "auto",
  },
  loginImage: {
    backgroundImage: "url('/static/images/login.jpg')",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  fullHeight: {
    height: "100vh",
  },
  textCenter: {
    textAlign: "center",
  },
  w50: {
    width: "50%",
  },
  w70: {
    width: "70%",
  },
  input: {
    WebkitBoxShadow: "0 0 0 1000px white inset",
  },
}));

const SplashScreen = dynamic(() => import('../../components/SplashScreen'));

const LoginPage = () => {
  const classes = useStyles();
  const { isError, isLoading, user } = useUser();
  const router = useRouter();

  if (isLoading) {
    return <SplashScreen />
  }

  if (user && !isLoading) {
    if (user.isAdmin !== undefined && user.isAdmin) {
      router.replace("/admin/dashboard");
    } else {
      router.replace("/dashboard");
    }
  }

  return (
    <Fragment>
      <Grid container component='main' className={classes.root}>
        <Grid item xs={12} sm={8} md={4} component={Paper} elevation={6}>
          <Grid
            container
            justify='center'
            direction='column'
            alignItems='center'
            className={classes.fullHeight}
          >
            <div className={classNames(classes.paper, classes.w50)}>
              <Grid
                item
                className={classNames(classes.textCenter, classes.w70)}
              >
                <Box mb={8}>
                  <Box><Logo /></Box>
                  <h1>Bejelentkezés</h1>
                  <small>
                    Ez egy biztonságos oldal, ezért kérlek jelentkezz be, hogy
                    minden funkciót élvezni tudj.
                  </small>
                </Box>
              </Grid>
              <Formik
                initialValues={{
                  email: "",
                  password: "",
                }}
                validationSchema={Yup.object().shape({
                  email: Yup.string()
                    .email("Must be a valid email")
                    .max(255)
                    .required("Email is required"),
                  password: Yup.string()
                    .max(255)
                    .required("Password is required"),
                })}
                onSubmit={async (
                  values,
                  { setErrors, setStatus, setSubmitting }
                ) => {
                  try {
                    const result = await signIn("credentials", {
                      redirect: false,
                      ...values,
                    });
                    trigger('/api/users/getUser');

                    if (result.error) {
                      setErrors({
                        submit: result.error,
                      });
                    } else {
                      setStatus({
                        success: true,
                      });
                    }

                    setSubmitting(true);
                  } catch (err) {
                    setErrors({
                      submit: err.response.data.errors,
                    });
                    setSubmitting(false);
                  }
                }}
              >
                {({
                  errors,
                  handleBlur,
                  handleChange,
                  handleSubmit,
                  isSubmitting,
                  touched,
                  values,
                }) => (
                  <form onSubmit={handleSubmit}>
                    <TextField
                      error={Boolean(touched.email && errors.email)}
                      helperText={touched.email && errors.email}
                      name='email'
                      label='Email'
                      autoFocus
                      fullWidth
                      type='text'
                      variant='outlined'
                      margin='normal'
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <TextField
                      error={Boolean(touched.password && errors.password)}
                      helperText={touched.password && errors.password}
                      fullWidth
                      variant='outlined'
                      name='password'
                      label='Password'
                      type='password'
                      margin='normal'
                      value={values.password}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    {errors.submit && (
                      <Box mt={3} mb={3}>
                        <FormHelperText className={classes.textCenter} error>
                          {errors.submit}
                        </FormHelperText>
                      </Box>
                    )}
                    <Button
                      type='submit'
                      color='primary'
                      fullWidth
                      variant='contained'
                      margin='space'
                      disabled={isSubmitting}
                    >
                      Bejelentkezés
                    </Button>
                  </form>
                )}
              </Formik>
            </div>
          </Grid>
        </Grid>
        <Grid item xs={false} sm={4} md={8} className={classes.loginImage} />
      </Grid>
    </Fragment>
  );
};

export default LoginPage;

LoginPage.layout = LoginLayout;
