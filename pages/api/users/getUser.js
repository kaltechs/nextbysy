import User from "../../../models/User";
import nextconnect from 'next-connect';
import { withAuth, withdB } from "../../../util/middlewares";

const handler = nextconnect({
  onError(error, req, res) {
    res.status(501).json({ message: `An error occured: ${error.message}` });
  },
  onNoMatch(req, res) {
    res.status(405).json({ message: `Method ${req.method} not allowed` });
  },
});
handler.use(withAuth());
handler.use(withdB());

export default handler.get(async (req, res) => {
  const userEmail = req.user.userEmail;
  
  const dbUser = await User.findOne({ email: userEmail }).select('-password');

  if (!dbUser) {
    res.status(404).json({ message: "User not found." });
    return;
  }

  return res.status(200).json(dbUser);
});
