import multer from "multer";
import multers3 from "multer-s3";
import { getSession } from "next-auth/client";
import { STATUS_CODE } from "../../../util/enums";
import path from "path";
import User from "../../../models/User";
import AWS from "aws-sdk";
import { connectToDatabase } from "../../../util/mongoDb";

export const config = {
  api: {
    bodyParser: false,
  },
};

const s3 = new AWS.S3({
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
  },
  region: "eu-central-1",
});

const createS3Upload = (email) => {
  const storage = multers3({
    s3: s3,
    bucket: "bysybucket",
    acl: "public-read",
    key: function (req, file, cb) {
      cb(
        null,
        `avatars/${email}/` +
          path.basename('avatar', path.extname(file.originalname)) +
          path.extname(file.originalname)
      );
    },
  });

  const upload = multer({
    storage: storage,
    limits: { fileSize: 2000000 },
    fileFilter: (req, file, cb) => {
      checkFileType(file, cb);
    },
  }).single("avatar");

  return upload;
};

// const createUpload = (email) => {
//   const storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//       const dir = `./public/var/uploads/${email}/tmp`;
//       fs.access(dir, (err) => {
//         if (err) {
//           return fs.mkdir(dir, { recursive: true }, (error) => cb(error, dir));
//         }
//         return cb(null, dir);
//       });
//     },
//     filename: function (req, file, cb) {
//       cb(null, file.originalname);
//     },
//   });

//   const upload = multer({
//     storage: storage,
//     limits: { fileSize: 1000000 },
//     fileFilter: (req, file, cb) => {
//       checkFileType(file, cb);
//     },
//   }).single("avatar");

//   return upload;
// };

const checkFileType = (file, cb) => {
  const fileTypes = /jpeg|jpg|png/;
  const extname = fileTypes.test(path.extname(file.originalname).toLowerCase());
  const mimeType = fileTypes.test(file.mimetype);

  if (mimeType && extname) {
    return cb(null, true);
  } else {
    cb("Error: Images only");
  }
};

export default async (req, res) => {
  if (req.method !== "POST") {
    return res
      .status(STATUS_CODE.MethodNotAllowed)
      .json({ message: "Unauthorized" });
  }

  const session = await getSession({ req: req });

  if (!session) {
    return res
      .status(STATUS_CODE.Unauthorized)
      .json({ message: "Unauthorized" });
  }

  const upload = createS3Upload(session.user.email);

  upload(req, res, async (err) => {
    let newUrl = "";

    if (err) {
      // fs.unlinkSync(req.file.path);
      return res.status(400).send(err);
    } else {
      try {
        await connectToDatabase();

        const dBuser = await User.findOne({ email: session.user.email });

        dBuser.avatar = req.file.location;
        await dBuser.save();

        newUrl = req.file.location;
      } catch (error) {
        res.status(401).json({
          message: error.message,
        });
      }
    }

    return res.status(200).json(newUrl);
  });
};
