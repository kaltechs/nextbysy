import { STATUS_CODE } from "../../../util/enums";
import { isValidObjectId } from "../../../util/mongoDb";
import User from "../../../models/User";
import nextConnect from 'next-connect';
import { withAuth, withdB } from '../../../util/middlewares';

const handler = nextConnect({
  onError(error, req, res) {
    res.status(501).json({ message: `An error occured: ${error.message}` });
  },
  onNoMatch(req, res) {
    res.status(405).json({ message: `Method ${req.method} not allowed` });
  },
});

handler.use(withAuth());
handler.use(withdB());

export default handler.patch(async (req, res) => {
  const userId = req.query.userId;

  if (!isValidObjectId(userId)) {
    res
      .status(STATUS_CODE.BadRequest)
      .json({ message: "Wrong user. Please refresh the page" });
    return;
  }

  const { email, firstName, lastName, phone, country, state, city } = req.body;

  try {
    const dbUser = await User.findOne({
      email: email,
    });

    if (!dbUser) {
      return res.status(STATUS_CODE.NotFound).json({
        message: "Can't find the profile",
      });
    }

    const updateFields = {
      firstName,
      lastName,
      phone,
      country,
      state,
      city,
    };

    const updatedUserResult = await User.findOneAndUpdate(
      { email: email },
      { $set: updateFields }
    );

    res.status(200).json(updatedUserResult);
  } catch (error) {
    res.status(500).json({ message: `Server error: ${error.message}` });
    return;
  }
});
