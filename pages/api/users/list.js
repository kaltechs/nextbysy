import User from "../../../models/User";
import nextconnect from "next-connect";
import { withAuth, withdB } from "../../../util/middlewares";

const handler = nextconnect({
  onError(error, req, res) {
    res.status(501).json({ message: `An error occured: ${error.message}` });
  },
  onNoMatch(req, res) {
    res.status(405).json({ message: `Method ${req.method} not allowed` });
  },
});
handler.use(withAuth());
handler.use(withdB());

const getListApi = handler.get(async (req, res) => {
  if (!req.user.isAuthenticated) {
    res.status(401).json({ message: "Unauthorized" });
    return;
  }

  if (!req.user.isAdmin) {
    res.status(401).json({ message: "Unauthorized" });
    return;
  }

  let usersList;

  try {
    usersList = await User.find().select("-password");
  } catch (error) {
    res.status(500).json({ message: `Server error: ${error.message}` });
    return;
  }

  res.status(200).json(usersList);
});

export default getListApi;
