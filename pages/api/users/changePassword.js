import { getSession } from "next-auth/client";
import User from "../../../models/User";
import { STATUS_CODE } from "../../../util/enums";
import {
  connectToDatabase,
  comparePassword,
  hashPassword,
} from "../../../util/mongoDb";

const handle = async (req, res) => {
  if (req.method !== "PATCH") {
    return res
      .status(STATUS_CODE.MethodNotAllowed)
      .json({ message: "Method not allowed" });
  }

  const session = await getSession({ req: req });

  if (!session) {
    return res
      .status(STATUS_CODE.Unauthorized)
      .json({ message: "Unauthorized!" });
  }

  const email = session.user.email;
  const { oldPassword, newPassword } = req.body;

  await connectToDatabase();
  const dbUser = await User.findOne({ email });

  if (!dbUser) {
    res.status(404).json({ message: "User not found!" });
    return;
  }

  const currentPassword = dbUser.password;
  const compareResult = await comparePassword(currentPassword, oldPassword);

  if (!compareResult) {
    return res.status(403).json({ message: "Invalid credentials" });
  }

  const hashedPassword = await hashPassword(newPassword);

  dbUser.password = hashPassword;
  const updateResult = await User.updateOne(
    { email },
    { $set: { password: hashedPassword } }
  );

  res.status(200).json({ message: "Password updated!" });
};

export default handle;
