import Expo from "../../../models/Expo/Expo";
import nextConnect from "next-connect";
import { withAuth, withdB } from "../../../util/middlewares";
import multer from "multer";
import multers3 from "multer-s3";
import AWS from "aws-sdk";
import { STATUS_CODE } from "../../../util/enums";
import path from "path";

const handler = nextConnect({
  onError(error, req, res) {
    res.status(501).json({ message: `An error occured: ${error.message}` });
  },
  onNoMatch(req, res) {
    res.status(405).json({ message: `Method ${req.method} not allowed` });
  },
});

handler.use(withAuth());
handler.use(withdB());

const s3 = new AWS.S3({
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
  },
  region: "eu-central-1",
});

//patch is actually a delete with req.body

export default handler.patch(async (req, res) => {
  if (!req.user.isAuthenticated) {
    res.status(401).json({ message: "Unauthorized" });
    return;
  }

  if (!req.user.isAdmin) {
    res.status(401).json({ message: "Unauthorized" });
    return;
  }

  try {
    const { _id } = req.body;
    const expoItem = await Expo.findById(_id);

    if (!expoItem) {
      return res
        .status(STATUS_CODE.BadRequest)
        .json({ message: "Expo not found." });
    }

    const splittedImageLocation = expoItem.expoimg.split("/");
    const avatarName = splittedImageLocation[splittedImageLocation.length - 1];

    s3.deleteObject(
      {
        Bucket: process.env.AWS_BUCKET,
        Key: `expo/${expoItem.email}/${avatarName}`,
      },
      (err, data) => {
        console.log(err);
      }
    );

    expoItem.expoimg = null;
    expoItem.save();

    res.status(200).json(expoItem);
  } catch (error) {
    res.status(500).json({ message: `An error occured ${error?.message}` });
  }
});
