import Expo from "../../../models/Expo/Expo";
import nextConnect from "next-connect";
import { withAuth, withdB } from "../../../util/middlewares";
import multer from "multer";
import multers3 from "multer-s3";
import AWS from "aws-sdk";
import { STATUS_CODE } from "../../../util/enums";
import path from "path";

export const config = {
  api: {
    bodyParser: false,
  },
};

const handler = nextConnect({
  onError(error, req, res) {
    res.status(501).json({ message: `An error occured: ${error.message}` });
  },
  onNoMatch(req, res) {
    res.status(405).json({ message: `Method ${req.method} not allowed` });
  },
});

handler.use(withAuth());
handler.use(withdB());

const s3 = new AWS.S3({
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
  },
  region: "eu-central-1",
});

export default handler.post(async (req, res) => {
  if (!req.user.isAuthenticated) {
    res.status(401).json({ message: "Unauthorized" });
    return;
  }

  if (!req.user.isAdmin) {
    res.status(401).json({ message: "Unauthorized" });
    return;
  }

  const storage = multers3({
    s3: s3,
    bucket: process.env.AWS_BUCKET,
    acl: "public-read",
    key: function (req, file, cb) {
      cb(
        null,
        `expo/${req.body.email}/` +
          path.basename("avatar", path.extname(file.originalname)) +
          path.extname(file.originalname)
      );
    },
  });

  const upload = multer({
    storage: storage,
    limits: { fileSize: 2000000 },
    fileFilter: (req, file, cb) => {
      checkFileType(file, cb, req);
    },
  }).single("avatar");

  const checkFileType = async (file, cb, req) => {
    const fileTypes = /jpeg|jpg|png/;
    const extname = fileTypes.test(
      path.extname(file.originalname).toLowerCase()
    );
    const mimeType = fileTypes.test(file.mimetype);

    if (mimeType && extname) {
      return cb(null, true);
    } else {
      cb("Error: Images only");
    }
  };

  upload(req, res, async (err) => {
    if (err) {
      return res.status(400).json({ message: err });
    } else {
      try {
        const { email } = req.body;

        await Expo.findOneAndUpdate(
          { email },
          { $set: { expoimg: req.file.location } }
        );

        return res.status(200).json({ message: req.file.location });
      } catch (err) {
        return res.status(500).json({
          message: err.message,
        });
      }
    }
  });
});
