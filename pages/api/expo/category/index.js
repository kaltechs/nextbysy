import { STATUS_CODE } from "../../../../util/enums";
import ExpoCategory from "../../../../models/Expo/Category";
import nextConnect from "next-connect";
import { withAuth, withdB } from "../../../../util/middlewares";

const handler = nextConnect({
  onError(error, req, res) {
    res.status(501).json({ message: `An error occured: ${error.message}` });
  },
  onNoMatch(req, res) {
    res.status(405).json({ message: `Method ${req.method} not allowed` });
  },
});

handler.use(withAuth());
handler.use(withdB());

const catRoutes = handler
  .get(async (req, res) => {
    try {
      const catListResult = await ExpoCategory.find();

      return res.status(200).json(catListResult);
    } catch (error) {
      res.status(500).json({ message: `Server error: ${error.message}` });
      return;
    }
  })
  .post(async (req, res) => {
    if (!req.user.isAuthenticated) {
      res.status(401).json({ message: "Unauthorized" });
      return;
    }

    if (!req.user.isAdmin) {
      res.status(401).json({ message: "Unauthorized" });
      return;
    }

    const { name, slug, color, _id } = req.body;

    try {
      if (_id !== "") {
        const catResult = await ExpoCategory.findOneAndUpdate(
          { _id: _id },
          { $set: { name, slug, color } },
          { new: true, upsert: true }
        );

        return res.json(catResult);
      }

      //check if existing
      const existingCategoryBySlug = await ExpoCategory.findOne({ slug });

      if (existingCategoryBySlug) {
        return res
          .status(STATUS_CODE.BadRequest)
          .json({ message: "Category already exists" });
      }

      //check if existing
      const existingCategoryByColor = await ExpoCategory.findOne({ color });

      if (existingCategoryByColor) {
        return res
          .status(STATUS_CODE.BadRequest)
          .json({ message: "Category with the same color already exists" });
      }

      var newModel = new ExpoCategory({
        name,
        slug,
        color,
      });

      newModel.save();

      return res.json(newModel);
    } catch (error) {
      res.status(500).json({ message: `Server error: ${error.message}` });
      return;
    }
  });

export default catRoutes;
