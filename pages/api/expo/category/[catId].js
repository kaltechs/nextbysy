import ExpoCategory from "../../../../models/Expo/Category";
import nextConnect from "next-connect";
import { withAuth, withdB } from "../../../../util/middlewares";

const handler = nextConnect({
  onError(error, req, res) {
    res.status(501).json({ message: `An error occured: ${error.message}` });
  },
  onNoMatch(req, res) {
    res.status(405).json({ message: `Method ${req.method} not allowed` });
  },
});

handler.use(withAuth());
handler.use(withdB());

export default handler.delete(async (req, res) => {
  if (!req.user.isAdmin) {
    res.status(401).json({ message: "Unauthorized" });
    return;
  }

  try {
    const idList = req.query.catId.split(",");
    const removeRes = await ExpoCategory.deleteMany({ _id: { $in: idList } });

    return res.status(200).json(removeRes);
  } catch (error) {
    res.status(500).json({ message: `Server error: ${error.message}` });
    return;
  }
});
