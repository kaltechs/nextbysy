import Expo from "../../../models/Expo/Expo";
import nextConnect from "next-connect";
import { withAuth, withdB } from "../../../util/middlewares";

const handler = nextConnect({
  onError(error, req, res) {
    res.status(501).json({ message: `An error occured: ${error.message}` });
  },
  onNoMatch(req, res) {
    res.status(405).json({ message: `Method ${req.method} not allowed` });
  },
});

handler.use(withAuth());
handler.use(withdB());


export default handler
  .get(async (req, res) => {
    if (!req.user.isAdmin) {
      res.status(401).json({ message: "Unauthorized" });
      return;
    }

    try {
      const expoId = req.query.expoId;
      const expoItem = await Expo.findById(expoId);

      return res.status(200).json(expoItem);
    } catch (error) {
      res.status(500).json({ message: `An error occured ${error?.message}` });
    }
  })
  .patch(async (req, res) => {
    if (!req.user.isAdmin) {
      res.status(401).json({ message: "Unauthorized" });
      return;
    }

    try {
      const expoId = req.query.expoId;

      const entries = Object.entries(req.body);
      const updateFields = {};

      entries.forEach((entry) => {
        if (entry[1] !== undefined) {
          updateFields[entry[0]] = entry[1];
        }
      });

      const updatResult = await Expo.findOneAndUpdate(
        { _id: expoId },
        { $set: updateFields },
        { new: true }
      );

      return res.json(updatResult);
    } catch (error) {
      res.status(500).json({ message: `Server error: ${error.message}` });
      return;
    }
  });
