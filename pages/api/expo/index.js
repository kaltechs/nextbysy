import multer from "multer";
import multers3 from "multer-s3";
import AWS from "aws-sdk";
import Expo from "../../../models/Expo/Expo";
import nextconnect from "next-connect";
import path from "path";
import { withAuth, withdB } from "../../../util/middlewares";

export const config = {
  api: {
    bodyParser: false,
  },
};

const handler = nextconnect({
  onError(error, req, res) {
    res.status(501).json({ message: `An error occured: ${error.message}` });
  },
  onNoMatch(req, res) {
    res.status(405).json({ message: `Method ${req.method} not allowed` });
  },
});

handler.use(withAuth());
handler.use(withdB());

const s3 = new AWS.S3({
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
  },
  region: "eu-central-1",
});

export default handler
  .get(async (req, res) => {
    let list;

    try {
      list = await Expo.find();
    } catch (error) {
      res.status(500).json({ message: `Server error: ${error.message}` });
      return;
    }

    res.status(200).json(list);
  })
  .post(async (req, res) => {
    if (!req.user.isAuthenticated) {
      res.status(401).json({ message: "Unauthorized" });
      return;
    }

    if (!req.user.isAdmin) {
      res.status(401).json({ message: "Unauthorized" });
      return;
    }

    const storage = multers3({
      s3: s3,
      bucket: process.env.AWS_BUCKET,
      acl: "public-read",
      key: function (req, file, cb) {
        cb(
          null,
          `expo/${req.body.email}/` +
            path.basename("avatar", path.extname(file.originalname)) +
            path.extname(file.originalname)
        );
      },
    });

    const upload = multer({
      storage: storage,
      limits: { fileSize: 2000000 },
      fileFilter: (req, file, cb) => {
        checkFileType(file, cb, req);
      },
    }).single("avatar");

    const checkFileType = async (file, cb, req) => {
      const fileTypes = /jpeg|jpg|png/;
      const extname = fileTypes.test(
        path.extname(file.originalname).toLowerCase()
      );
      const mimeType = fileTypes.test(file.mimetype);

      const existingExpoItem = await Expo.findOne({ email: req.body.email });

      if (mimeType && extname && !existingExpoItem) {
        return cb(null, true);
      } else {
        if (existingExpoItem) {
          cb("Email address in use.");
        }
        cb("Error: Images only");
      }
    };

    upload(req, res, async (err) => {
      if (err) {
        return res.status(400).json({ message: err });
      } else {
        try {
          const {
            name,
            companyName,
            email,
            category,
            cardContent,
            intro,
            curiosity,
            isDraft,
            isPublished,
            spokenLanguages,
            headquarters,
            prices,
            introVideoUrl,
          } = req.body;

          const newExpoModel = new Expo({
            name,
            companyName,
            email,
            category,
            cardContent,
            intro,
            curiosity,
            isDraft,
            isPublished,
            spokenLanguages: JSON.parse(spokenLanguages),
            headquarters: JSON.parse(headquarters),
            prices: JSON.parse(prices),
            expoimg: req.file.location,
            introVideoUrl,
          });

          newExpoModel.save();
          return res.status(200).json({ message: "Expo Item was saved" });
        } catch (error) {
          res.status(500).json({
            message: error.message,
          });
        }
      }
    });
  });
