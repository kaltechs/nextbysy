import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import { comparePassword } from "../../../util/auth";
import User from "../../../models/User";
import { connectToDatabase } from "../../../util/mongoDb";
import { signIn } from "next-auth/client";

export default NextAuth({
  session: {
    jwt: true,
  },
  jwt: {
    secret: process.env.JWT_SECRET,
  },
  providers: [
    Providers.Credentials({
      async authorize(credentials) {
        await connectToDatabase();

        const { email, password } = credentials;

        const dbUser = await User.findOne({ email });

        if (!dbUser) {
          throw new Error("Invalid credentials");
        }

        const compareResult = await comparePassword(dbUser.password, password);

        if (!compareResult) {
          throw new Error("Invalid credentials");
        }

        return {
          email: dbUser.email,
          fullName: dbUser.fullName,
          firstName: dbUser.firstName,
          lastName: dbUser.lastName,
          isClient: dbUser.isClient,
          isAdmin: dbUser.isAdmin,
          avatar: dbUser.avatar,
        };
      },
    }),
  ],
  callbacks: {
    async jwt(token, user, account, profile, isNewUser) {
      
      if (user?.email) {
        token.email = user.email;
      }

      if (user?.fullName) {
        token.fullName = user.fullName;
      }

      if (user?.firstName) {
        token.firstName = user.firstName;
      }

      if (user?.lastName) {
        token.lastName = user.lastName;
      }

      if (user?.isClient) {
        token.isClient = user.isClient;
      }

      if (user?.isAdmin) {
        token.isAdmin = user.isAdmin;
      }

      if (user?.avatar) {
        token.avatar = user.avatar;
      }

      return token;
    },
    async session(session, token) {
      session.user = {
        ...session.user, 
        ...token
      }

      return session;
    }
  },
  pages: {
    signIn: "/auth/login",
  },
});
