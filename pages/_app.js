import React from "react";
import Head from "next/head";
import { Provider } from "next-auth/client";
import { ThemeProvider } from "@material-ui/core/styles";
import { createTheme } from "../theme";
import 'react-perfect-scrollbar/dist/css/styles.css';

import '../scss/style.default.scss';

export default function MyApp(props) {
  const { Component, pageProps } = props;

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  const theme = createTheme();

  const Layout = Component.layout || (({ children }) => <>{children}</>);

  return (
    <>
      <Head>
        <meta
          name='viewport'
          content='width=device-width, initial-scale=1, shrink-to-fit=no'
        />
        <title>Before You Say Yes</title>
      </Head>
      <Provider session={pageProps.session}>
        <ThemeProvider theme={theme}>
          <Layout {...pageProps} title={Component.title}>
            <Component {...pageProps} />
          </Layout>
        </ThemeProvider>
      </Provider>
    </>
  );
}
