import dynamic from 'next/dynamic';

const DashboardLayout = dynamic(() => import('../../layouts/AdminDashboardLayout'));

const DashboardPage = () => {
    return (
        <div>Dashboard for Admin</div>
    )
}

export default DashboardPage;

DashboardPage.layout = DashboardLayout;