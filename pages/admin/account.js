import { Box, Container, Divider, Tab, Tabs } from "@material-ui/core";
import { Fragment, useState } from "react";
import dynamic from "next/dynamic";
import Header from "../../components/AdminDashboard/Header";
import AdminDashboard from '../../layouts/AdminDashboardLayout';

const SplashScreen = dynamic(() => import("../../components/LoadingScreen"));
const GeneralSettings = dynamic(
  () =>
    import(
      "../../components/AdminDashboard/Account/GeneralSettings/GeneralSettings"
    ),
  { loading: () => <SplashScreen /> }
);

const Security = dynamic(
  () => import("../../components/AdminDashboard/Account/Security"),
  { loading: () => <SplashScreen /> }
);
const Notifications = dynamic(
  () => import("../../components/AdminDashboard/Account/Notifications")
);

const AccountPage = () => {
  const tabs = [
    { value: "general", label: "General" },
    { value: "notifications", label: "Notifications" },
    { value: "security", label: "Security" },
  ];

  const [currentTab, setCurrentTab] = useState("general");

  const handleTabChange = (e, value) => {
    setCurrentTab(value);
  };

  return (
    <Fragment>
      <Container maxWidth='lg'>
        <Header current='Settings' />
        <Box mt={3}>
          <Tabs
            onChange={handleTabChange}
            scrollButtons='auto'
            value={currentTab}
            variant='scrollable'
            textColor='primary'
            indicatorColor='primary'
          >
            {tabs.map((tab) => (
              <Tab key={tab.value} label={tab.label} value={tab.value} />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box mt={3}>
          {currentTab === "general" && <GeneralSettings />}
          {currentTab === "notifications" && <Notifications />}
          {currentTab === "security" && <Security />}
        </Box>
      </Container>
    </Fragment>
  );
};

AccountPage.layout = AdminDashboard;

export default AccountPage;
