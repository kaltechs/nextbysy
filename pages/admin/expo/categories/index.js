import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import { useState } from "react";
import Header from "../../../../components/AdminDashboard/Header";
// import CategoryManagement from "../../../../components/AdminDashboard/Expo/Category/CategoryManagement";
import CategoryListView from "../../../../components/AdminDashboard/Expo/Category/CategoryListView";
import LoadingScreen from "../../../../components/LoadingScreen";
import { fetcher } from "../../../../util/fetcher";
import useSwr from "swr";
import dynamic from "next/dynamic";

const AdminDashboard = dynamic(() =>
  import("../../../../layouts/AdminDashboardLayout")
);

const CategoryManagement = dynamic(() =>
  import(
    "../../../../components/AdminDashboard/Expo/Category/CategoryManagement"
  )
);

const initialData = {
  _id: "",
  name: "",
  slug: "",
  color: "#FFF",
};

const ExpoCategory = () => {
  const { data: expoData } = useSwr("/api/expo/category", fetcher);
  const [editData, setEditData] = useState(initialData);

  return (
    <Container maxWidth={false}>
      <Header current='Expo Category Management' />
      <Box mt={3}>
        {!expoData ? (
          <LoadingScreen />
        ) : (
          <Grid container spacing={3}>
            <Grid item lg={4} md={6} xl={4} xs={12}>
              <CategoryManagement
                categories={expoData}
                toEditData={editData}
                setEditData={setEditData}
              />
            </Grid>
            <Grid item lg={8} md={6} xl={8} xs={12}>
              <CategoryListView categories={expoData} editData={setEditData} />
            </Grid>
          </Grid>
        )}
      </Box>
    </Container>
  );
};

ExpoCategory.layout = AdminDashboard;

export default ExpoCategory;
