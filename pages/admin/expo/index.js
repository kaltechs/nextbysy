import dynamic from "next/dynamic";
import { fetcher } from "../../../util/fetcher";
import useSwr from "swr";
import { Box, Container, Grid } from "@material-ui/core";
import Header from "../../../components/AdminDashboard/Header";

const AdminLayout = dynamic(() =>
  import("../../../layouts/AdminDashboardLayout")
);

const LoadingScreen = dynamic(() =>
  import("../../../components/LoadingScreen")
);

const ExpoList = dynamic(() =>
  import("../../../components/AdminDashboard/Expo/ExpoList")
);

const ExpoListPage = () => {
  const { data: expoList } = useSwr("/api/expo", fetcher);

  return (
    <Container maxWidth={false}>
      <Header current='Expo List' />
      <Box mt={3}>
        {!expoList ? (
          <LoadingScreen />
        ) : (
          <Grid container spacing={3}>
            <Grid item md={12} xs={12}>
              <ExpoList expoList={expoList} />
            </Grid>
          </Grid>
        )}
      </Box>
    </Container>
  );
};

ExpoListPage.layout = AdminLayout;

export default ExpoListPage;
