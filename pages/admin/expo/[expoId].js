import { useRouter } from "next/router";
import dynamic from "next/dynamic";
import { fetcher } from "../../../util/fetcher";
import useSWR from "swr";
import { Container } from "@material-ui/core";
import Header from "../../../components/AdminDashboard/Header";

const AdminDashboard = dynamic(() =>
  import("../../../layouts/AdminDashboardLayout")
);

const LoadingScreen = dynamic(() =>
  import("../../../components/LoadingScreen")
);

const EditTabs = dynamic(() =>
  import("../../../components/AdminDashboard/Expo/ExpoEdit"), {
    ssr: false
  }
);

const editExpo = () => {
  const router = useRouter();
  const { expoId } = router.query;


  if (!expoId) {
    return <LoadingScreen />;
  }

  const { data } = useSWR(`/api/expo/${router.query.expoId}`, fetcher);

  return (
    <>
      {!data ? (
        <LoadingScreen />
      ) : (
        <>
          <Container maxWidth='lg'>
            <Header current={`Edit - ${data.name}`} />
            <EditTabs data={data} />
          </Container>
        </>
      )}
    </>
  );
};

editExpo.layout = AdminDashboard;

export default editExpo;
