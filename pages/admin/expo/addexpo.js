import Tabs from "@material-ui/core/Tabs";
import Box from "@material-ui/core/Box";
import Tab from "@material-ui/core/Tab";
import Container from "@material-ui/core/Container";
import Divider from "@material-ui/core/Divider";
import { useState } from "react";
import Header from "../../../components/AdminDashboard/Header";
import dynamic from "next/dynamic";
import "react-quill/dist/quill.snow.css";
import LoadingScreen from '../../../components/LoadingScreen';

const initialExpoData = {
  email: 'kaltechs@kaltechs.net',
  name: "Kerekes Sandor",
  companyName: "kaltechs",
  category: "zene",
  cardContent: "Ez a card Content resz",
  intro: "Ez az Intro resz",
  curiosity: "Ez az erdekesseg resz",
  isDraft: true,
  isPublished: false,
  expoimg: null,
  avatar: null,
  spokenLanguages: [
    {
      name: "Spanish",
      value: 5,
    },
    {
      name: "Hungarian",
      value: 9,
    },
    {
      name: "Romanian",
      value: 4,
    },
  ],
  headquarters: [
    {
      name: "Budapest",
      value: "12 ker.",
    },
    {
      name: "Kolozsvar",
      value: "Monostor",
    },
  ],
  prices: [
    {
      name: "Kiszallas",
      value: "15.000ft - 25.000ft",
    },
    {
      name: "Szallas",
      value: "35.000ft",
    },
  ],
  introVideoUrl: 'https://www.youtube.com/watch?v=bs4F_VJwmS0&list=PLOWSbqDrrI5RV1I7JKTWQnvJpjgqC_yzD'
};

const tabs = [
  { value: "general", label: "General" },
  { value: "personal", label: "Personal" },
  { value: "result", label: "Summary" },
];

const AdminDashboard = dynamic(() =>
  import("../../../layouts/AdminDashboardLayout")
);

const GeneralSettings = dynamic(
  () =>
    import(
      "../../../components/AdminDashboard/Expo/ExpoCreation/GeneralSettings"
    ),
  {
    ssr: false,
  }
);

const PersonalSettings = dynamic(
  () =>
    import(
      "../../../components/AdminDashboard/Expo/ExpoCreation/PersonalSettings"
    ),
  {
    loading: () => <LoadingScreen />
  },
);

const ExpoCreationResult = dynamic(
  () =>
    import(
      "../../../components/AdminDashboard/Expo/ExpoCreation/ExpoCreationResult"
    ),
  {
    ssr: false,
  }
);

const AddExpo = () => {
  const [currentTab, setCurrentTab] = useState("general");
  const [expoData, setExpoData] = useState(initialExpoData);

  const handleTabChange = (e, value) => {
    setCurrentTab(value);
  };

  return (
    <>
      <Container maxWidth='lg'>
        <Header current='Add new Expo person' />
        <Box mt={3}>
          <Tabs
            onChange={handleTabChange}
            scrollButtons='auto'
            value={currentTab}
            variant='scrollable'
            textColor='primary'
            indicatorColor='primary'
          >
            {tabs.map((tab) => (
              <Tab key={tab.value} label={tab.label} value={tab.value} />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box mt={3}>
          {currentTab === "general" && (
            <GeneralSettings expoData={expoData} setExpoData={setExpoData} />
          )}
        </Box>
        <Box mt={3}>
          {currentTab === "personal" && (
            <PersonalSettings
              personalData={expoData}
              setPersonalData={setExpoData}
            />
          )}
        </Box>
        <Box mt={3}>
          {currentTab === "result" && (
            <ExpoCreationResult expoData={expoData} setExpoData={setExpoData} />
          )}
        </Box>
      </Container>
    </>
  );
};

AddExpo.layout = AdminDashboard;

export default AddExpo;
