import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import dynamic from "next/dynamic";
import Header from "../../../components/AdminDashboard/Header";
import { FormHelperText } from "@material-ui/core";
import  { useRequest } from "../../../util/fetcher";

const AdminDashbaord = dynamic(() => import('../../../layouts/AdminDashboardLayout'))

const UsersListView = () => {
  const UserListGrid = dynamic(() =>
    import("../../../components/AdminDashboard/Users/UsersListGrid")
  );

  const LoadingScreen = dynamic(() =>
    import("../../../components/LoadingScreen")
  );

  const { data: usersList, error } = useRequest({url: "/api/users/list"});

  return (
    <Container maxWidth={false}>
      <Header current='Users List' />
      {!usersList && !error ? (
        <LoadingScreen />
      ) : (
        <Box mt={3}>
          {error ? (
            <FormHelperText error>{error.response.data.message}</FormHelperText>
          ) : (
            <UserListGrid users={usersList} />
          )}
        </Box>
      )}
    </Container>
  );
};

UsersListView.layout = AdminDashbaord;

export default UsersListView;
