import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";

const propTypes = {
  endpoint: PropTypes.string,
  params: PropTypes.any,
};

const initialValues = {
  endpoint: null,
  params: null,
};

const useGetApiClient = (endpoint, params) => {
  const [data, setData] = useState(null);

  const [error, setError] = useState(null);

  const [pending, setPending] = useState(true);

  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    if (endpoint) {
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };

      axios
        .get(endpoint, config)
        .then((resp) => {
          setError(null);
          setData(resp.data);
          setPending(false);
          setRefresh(false);
        })
        .catch((error) => {
          setError(error);
          setPending(false);
        });
    }
  }, [endpoint, refresh]);

  return {
    data,
    pending,
    error,
    setRefresh,
  };
};

useGetApiClient.propTypes = propTypes;

export default useGetApiClient;
