import useSWR from "swr";
import { fetcher } from "../util/fetcher";
import {useRouter} from 'next/router';

export const useUser = () => {
  const router = useRouter();

  const { data, error } = useSWR(`/api/users/getUser`, fetcher);

  if (error && error.response.data.message === "User not found." && !router.pathname.startsWith('/auth')) {
    router.replace('/auth/login');
  }

  if (!error && data && data.isAdmin && router.pathname.startsWith('/auth')){
    // router.replace('/admin/dashboard');
  }

  if (!error && data && data.isCLient && router.pathname.startsWith('/auth')){
    router.replace('/dashboard');
  }

  return {
    user: !error ? data : null,
    isLoading: !error && !data,
    isError: error,
  };
};
